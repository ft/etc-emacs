;; -*- lexical-binding: t; -*-

(setq gc-cons-threshold (* 50 1024 1024))

(add-hook 'after-init-hook (lambda ()
                             (setq gc-cons-threshold (* 4 1024 1024))))

(setq-default load-prefer-newer t)
(setq-default custom-file
              (expand-file-name "customize.el" user-emacs-directory))

(setq-default default-frame-alist
              '((background-color       . "black")
                (foreground-color       . "#bebebe")
                (fullscreen             . maximized)
                (horizontal-scroll-bars . nil)
                (vertical-scroll-bars   . nil)
                (menu-bar-lines         . 0)
                (tool-bar-lines         . 0)
                (right-divider-width    . 1)))

(dolist (dir '("themes" "private"))
  (add-to-list 'load-path (expand-file-name dir user-emacs-directory)))

(dolist (dir '("~/.local/bin"
               "~/.ghcup/bin"
               "~/.cargo/bin"))
  (let ((lbin (expand-file-name dir)))
    (when (file-accessible-directory-p lbin)
      (setq exec-path (cons lbin exec-path))
      (setenv "PATH" (string-join exec-path path-separator)))))
