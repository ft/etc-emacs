(require 'ft-colours)
(deftheme ft-light "Light-backgrounded colour scheme. -ft")
(ft/define-colour-scheme t)
(provide-theme 'ft-light)
