(require 'ft-colours)
(deftheme ft-dark "Dark-backgrounded colour scheme. -ft")
(ft/define-colour-scheme nil)
(provide-theme 'ft-dark)
