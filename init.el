;; -*- lexical-binding: t; -*-

;; early-init.el sets custom-file to something that's not this file, so load it
;; early in here to have custom values have their desired effects.
(when (file-exists-p custom-file)
  (load custom-file))

;; Setup package.el early so we can get use-package if we need to.
(require 'package)
;; (setq package-archive-priorities '(("gnu"    . 99)
;;                                    ("nongnu" . 80)
;;                                    ("stable" . 70)
;;                                    ("melpa"  . 20)))
(let ((repos (mapcar #'car package-archives)))
  (dolist (repo '(("gnu"    . "http://elpa.gnu.org/packages/")
                  ("nongnu" . "http://elpa.nongnu.org/nongnu/")
                  ("stable" . "https://stable.melpa.org/packages/")
                  ("melpa"  . "http://melpa.org/packages/")))
    (unless (member (car repo) repos)
      (add-to-list 'package-archives repo))))

;; use-package setup is next; after that, everything is handled by it.
(unless (fboundp 'use-package)
  (package-install 'use-package))

(defvar ft/use-package-debug nil)
(defvar use-package-verbose ft/use-package-debug)
(defvar use-package-expand-minimally (not ft/use-package-debug))
(defvar use-package-minimum-reported-time 0.025)
(require 'use-package)

;; Private extensions and low-level emacs setup first
(use-package ft-utilities)
(use-package ft-core)

;; Server setup for GUI instances.
(use-package server
  :if (and (not noninteractive) (memq (ft/host-system) '(x-win 'ms-win 'mac)))
  :preface
  (defun ft/maybe-start-server ()
    (if (server-running-p server-name)
        (message "Another server running on %s; leaving it alone."
                 server-name)
      (message "Starting emacs server: %s" server-name)
      (server-start)))
  (setq server-kill-new-buffers nil)
  (setq server-temp-file-regexp "\\`/tmp/Re\\|/draft\\|COMMIT_EDITMSG\\|git-rebase-todo\\'")
  :init (add-hook 'after-init-hook #'ft/maybe-start-server))

;; Misc extensions from private code
(use-package select-window-hook
  :config
  (insinuate-select-window-hook)
  (defun ft/want-tmux-title ()
    "Determine if the running emacs instance should try to update
tmux's status line."
    (and (eq (ft/host-system) 'console)
         (string-match "^screen" (ft/term))))
  (defun ft/tmux-set-title ()
    (send-string-to-terminal (format "\ekemacs(%s)\e\\" (buffer-name))))
  (when (ft/want-tmux-title)
    (add-hook 'window-configuration-change-hook #'ft/tmux-set-title)
    (add-hook 'emacs-startup-hook #'ft/tmux-set-title)
    (add-hook 'select-window-hook #'ft/tmux-set-title)))

(use-package keychain-environment
  :commands (keychain-update))

(use-package variable-edit
  :after paredit
  :commands (variable-edit variable-edit-in-scratch))

;; Evil next. Modal editing goodness. We need diminish too, so we can use it in
;; setting up evil-collection.
(use-package diminish
  :ensure t)

(use-package evil
  :ensure t
  :after package
  :init
  (defvar evil-want-keybinding nil)
  (defvar evil-want-integration t)
  (defvar evil-want-C-u-scroll t)
  (defvar evil-echo-state nil)
  (defvar evil-symbol-word-search t)
  (defvar evil-indent-convert-tabs nil)
  ;;(defvar evil-undo-system 'undo-tree)
  (defvar evil-normal-state-tag   "n")
  (defvar evil-emacs-state-tag    "e")
  (defvar evil-visual-state-tag   "v")
  (defvar evil-insert-state-tag   "i")
  (defvar evil-operator-state-tag "o")
  (defvar evil-motion-state-tag   "m")
  (defvar evil-replace-state-tag  "r")
  :config
  (evil-mode 1)
  (defun ft/bol+push-button ()
    (interactive)
    (beginning-of-line-text)
    (push-button))
  (evil-define-key 'normal package-menu-mode-map
    (kbd "RET") #'ft/bol+push-button)
  (dolist (state '(insert replace))
    (evil-global-set-key state (kbd "C-d") #'evil-normal-state))
  (custom-set-variables '(evil-undo-system 'undo-redo))
  (setq evil-default-cursor '("white" box))
  (setq evil-normal-state-cursor '("green" box))
  (setq evil-insert-state-cursor '("green" bar))
  (setq evil-replace-state-cursor '("red" hbar))
  (setq evil-emacs-state-cursor nil)
  (setq evil-visual-state-cursor '("cyan" box))
  (setq evil-motion-state-cursor '("magenta" (hbar . 5)))
  (setq evil-operator-state-cursor '("green" hollow)))

(use-package evil-collection
  :ensure t
  :after evil
  :config
  (delq 'gnus evil-collection-mode-list)
  (evil-collection-init)
  (diminish 'evil-collection-unimpaired-mode))

(use-package ft-space-key
  :after evil
  :config
  (ft/bind-space (list evil-normal-state-map
                       evil-motion-state-map))
  ;; In insertion states like insert, replace, and emacs dispatch into the
  ;; space map via Meta-Space. Clumsier but works.
  (define-key global-map (kbd "M-SPC") ft/space-map)

  (defun ft/buffer-file-name ()
    (interactive)
    (message "Buffer file name: %s" buffer-file-name))

  (define-key ft/space-map "f" 'ft/buffer-file-name)
  (define-key ft/space-map "p" 'ft/current-position)
  (define-key ft/space-map "u" 'universal-argument))

(use-package xref
  :config
  (evil-global-set-key 'normal (kbd "g t") 'xref-find-definitions)
  (evil-global-set-key 'normal (kbd "g b") 'xref-go-back)
  (evil-global-set-key 'normal (kbd "g B") 'xref-go-forward))

;; Looks go next: Themes, modeline, and TUI/GUI setup.
(use-package custom
  :bind ("<f4>" . #'ft/toggle-colours)
  :config
  (setq modus-themes-common-palette-overrides
        '((comment yellow-faint)
          (string green-faint)
          (bg-paren-match bg-blue-intense))))

(use-package ft-modeline
  :config (ft/line:mode-and-header))

(use-package ft-gui
  :demand t
  :after custom
  :bind ("<f12>" . #'ft/update-frame-parameters)
  :config
  (ft/initialise-gui)
  (ft/dark-colours))

(use-package hl-line
  :commands (hl-line-mode)
  :bind ("<f2>" . #'hl-line-mode)
  :init (ft/register-to-hooks
         #'hl-line-mode
         (mapcar #'ft/mode-to-hook '(compilation-mode
                                     dired-mode
                                     package-menu-mode))))

(use-package whitespace
  :commands whitespace-mode
  :bind ("<f1>" . #'whitespace-mode)
  :custom
  (whitespace-display-mappings '((newline-mark ?\n [?⋄ ?\n] [?$ ?\n])
                                 (tab-mark     ?\t [?› ?\t] [?> ?\t])))
  (whitespace-style '(face
                      lines-tail trailing
                      newline newline-mark
                      ;; Unfortunately, there's a display problem with
                      ;; whitespace-mode and cases where a tab character
                      ;; occupies exactly one space. Otherwise enabling the
                      ;; following would be nice:
                      ;;
                      ;; tab-mark
                      tabs))
  :init
  (add-hook 'package-menu-mode-hook
            (lambda ()
              (whitespace-mode -1)
              (hl-line-mode 1))))

(use-package evil-whitespace
  :bind (:map ft/space-map ("S" . #'evil-whitespace-toggle))
  :after (evil whitespace)
  :config
  (evil-whitespace-setup)
  (ft/register-to-hooks #'evil-whitespace-mode
                        (mapcar #'ft/mode-to-hook
                                '(prog-mode git-commit-mode text-mode))))

(use-package font-lock
  :config (setq font-lock-maximum-decoration '((c-mode . 2)
                                               (c++-mode . 2)
                                               (t . t))))

(use-package paren
  :commands show-paren-mode
  :config
  (show-paren-mode t)
  (setq show-paren-delay 0)
  (setq show-paren-style 'parenthesis))

(use-package rainbow-mode
  :ensure t)

;; Frame/Window control
(use-package winner
  :after evil
  :config (winner-mode)
  :custom ((winner-dont-bind-my-keys t)
           (winner-ring-size 256))
  :bind (:map evil-window-map
              ("u" . #'winner-undo)
              ("U" . #'winner-redo)))

(use-package windmove
  :after evil
  :bind (:map evil-window-map
              ("<up>"    . #'windmove-up)
              ("<down>"  . #'windmove-down)
              ("<left>"  . #'windmove-left)
              ("<right>" . #'windmove-right)))

(use-package transpose-frame
  :ensure t
  :after ft-space-key
  :commands (transpose-frame
             flip-frame
             flop-frame
             rotate-frame
             rotate-frame-clockwise
             rotate-frame-anticlockwise)
  :bind (:map ft/space-map
              ("F t" . #'transpose-frame)
              ("F f" . #'flip-frame)
              ("F F" . #'flop-frame)
              ("F r" . #'rotate-frame)
              ("F c" . #'rotate-frame-clockwise)
              ("F a" . #'rotate-frame-anticlockwise)))

(use-package ace-window
  :ensure t
  :after ft-space-key
  :commands (ace-window ace-swap-window)
  :bind (("C-x o" . #'ace-window)
         :map ft/space-map
         ("j" . #'ace-window)
         ("k" . #'ace-swap-window))
  :config
  (setq aw-scope 'frame)
  (setq aw-keys '(?f ?j ?d ?k ?s ?l ?a ?/ ?g ?h ?1 ?2 ?3 ?4 ?5 ?6 ?7 ?8 ?9 ?0)))

;; Buffer navigation
(use-package ibuffer
  :bind ("C-x C-b" . #'ibuffer)
  :config
  (setq ibuffer-formats '((mark modified read-only " "
                                (name 18 18 :left :elide)
                                " "
                                (size 9 -1 :right)
                                " "
                                (mode 16 16 :left :elide)
                                " "
                                (vc-status 16 16 :left)
                                " " filename-and-process)
                          (mark " "
                                (name 16 -1)
                                " " filename)))
  (add-hook 'ibuffer-mode-hook
            (lambda () (hl-line-mode 1))))

(use-package ibuf-ext
  :after ibuffer
  :config (setq ibuffer-show-empty-filter-groups nil))

(use-package ibuffer-vc
  :ensure t
  :after (evil ibuf-ext)
  :config
  (evil-define-key 'normal ibuffer-mode-map
    (kbd "s V") #'ibuffer-vc-set-filter-groups-by-vc-root)
  (add-hook 'ibuffer-hook
            (lambda ()
              (ibuffer-vc-set-filter-groups-by-vc-root)
              (unless (eq ibuffer-sorting-mode 'alphabetic)
                (ibuffer-do-sort-by-alphabetic)))))

;; Completion setup
(use-package vertico
  :ensure t
  :after ft-space-key
  :init
  (defun ft/mini-buffer-up-directory (arg)
    (interactive "p")
    (if (string-match-p "/." (minibuffer-contents))
        (zap-up-to-char (- arg) ?/)
      (delete-minibuffer-contents)))
  (defun ft/mini-buffer-root-dir (arg)
    (interactive "p")
    (delete-minibuffer-contents)
    (insert "/"))
  (defun ft/mini-buffer-home-dir (arg)
    (interactive "p")
    (delete-minibuffer-contents)
    (insert "~/"))
  :config
  (vertico-mode 1)
  (setq completion-in-region-function
        (lambda (&rest args)
          (apply (if vertico-mode
                     #'consult-completion-in-region
                   #'completion--in-region)
                 args)))
  :bind (:map vertico-map
              ("M-+" . #'ft/mini-buffer-root-dir)
              ("M--" . #'ft/mini-buffer-home-dir)
              ("M-w" . #'ft/mini-buffer-up-directory)
              ("M-c" . #'delete-minibuffer-contents)))

(use-package hippie-exp
  :commands (hippie-expand)
  :bind ("M-/" . #'hippie-expand)
  :config
  (setq hippie-expand-try-functions-list
        '(try-expand-dabbrev-visible
          try-expand-dabbrev
          try-complete-file-name-partially
          try-complete-file-name
          try-expand-dabbrev-all-buffers
          try-expand-all-abbrevs
          try-expand-list)))

(use-package corfu
  :ensure t
  :custom
  (corfu-cycle t)
  (corfu-auto nil)
  (corfu-separator ?\s)
  (corfu-quit-no-match t)
  (corfu-preview-current nil)
  (corfu-preselect-first t)
  (corfu-on-exact-match 'insert)
  (corfu-echo-documentation nil)
  (corfu-scroll-margin 3)
  :config
  (dolist (state '(insert emacs))
    (evil-global-set-key state (kbd "C-p") #'completion-at-point))
  (define-key corfu-map [remap completion-at-point] #'corfu-previous)
  (define-key corfu-map [remap evil-complete-previous] #'corfu-previous)
  (define-key corfu-map [remap evil-complete-next] #'corfu-next)
  ;; Work around a problem in corfu: When the parent frame's font size changes
  ;; (which is what we're doing here), the internal ‘corfu--frame’ parameter,
  ;; if corfu was active before with another font, still holds the old font
  ;; setup. That causes the sub frame sizes to be off. Updating the frame's
  ;; font parameter to the one we're applying to the parent fixes the problem.
  ;; See https://github.com/minad/corfu/issues/286 for details.
  (require 'nadvice)
  (advice-add 'corfu--make-frame :before
              (lambda (&rest r)
                (when (bound-and-true-p corfu--frame)
                  (set-frame-parameter corfu--frame 'font
                                  (frame-parameter nil 'font)))
                (when (bound-and-true-p corfu-popupinfo--frame)
                  (set-frame-parameter corfu-popupinfo--frame 'font
                                  (frame-parameter nil 'font)))))
  :init (global-corfu-mode))

(use-package corfu-terminal
  :after corfu
  :ensure t
  :config
  (unless (display-graphic-p)
    (corfu-terminal-mode +1)))

(use-package cape
  :ensure t
  :after corfu
  :bind (("M-* t"  . complete-tag)
         ("M-* d"  . cape-dabbrev)
         ("M-* h"  . cape-history)
         ("M-* f"  . cape-file)
         ("M-* k"  . cape-keyword)
         ("M-* s"  . cape-symbol)
         ("M-* a"  . cape-abbrev)
         ("M-* i"  . cape-ispell)
         ("M-* l"  . cape-line)
         ("M-* w"  . cape-dict)
         ("M-* \\" . cape-tex)
         ("M-* _"  . cape-tex)
         ("M-* ^"  . cape-tex)
         ("M-* &"  . cape-sgml)
         ("M-* r"  . cape-rfc1345))
  :config
  (setq cape-file-directory-must-exist nil))

(use-package marginalia
  :ensure t
  :after vertico
  :config (marginalia-mode 1)
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy
                           marginalia-annotators-light nil))
  (marginalia-separator-threshold 120)
  (marginalia-truncate-width 80))

(use-package embark
  :ensure t
  :commands (embark-act)
  :custom
  (embark-indicators '(embark-mixed-indicator))
  (embark-mixed-indicator-delay 1.0)
  :bind ("M-'" . #'embark-act))

(use-package embark-consult
  :ensure t
  :after embark)

(use-package embark-vc
  :disabled t
  :ensure t
  :after embark)

(use-package orderless
  :ensure t
  :custom (completion-styles '(orderless)))

(use-package consult
  :ensure t
  :commands (consult-git-grep
             consult-ls-git-ls-files
             consult-ls-git-ls-status
             consult-locate
             consult-org-heading
             consult-line)
  :after ft-space-key
  :bind (:map ft/space-map
              ("c g" . #'consult-git-grep)
              ("c f" . #'consult-ls-git-ls-files)
              ("c s" . #'consult-ls-git-ls-status)
              ("c l" . #'consult-locate)
              ("c o" . #'consult-org-heading)
              ("s"   . #'consult-line))
  :config
  (setq consult-line-start-from-top t))

(use-package consult-ag
  :ensure t
  :after ft-space-key consult
  :bind (:map ft/space-map ("c a" . #'consult-ag)))

(use-package consult-eglot
  :ensure t
  :after (ft-space-key consult))

(use-package consult-flyspell
  :ensure t
  :after (ft-space-key consult))

(use-package consult-ls-git
  :ensure t
  :after (ft-space-key consult)
  :bind (:map ft/space-map ("c G" . #'consult-ls-git)))

;; Search engines
(use-package ag
  :ensure t
  :commands ag ag-regexp)

;; Template (snippet) system
(use-package tempel
  :ensure t
  :commands (tempel-complete)
  :bind ("M-+" . #'tempel-complete))

(use-package ft-templates
  :after tempel)

;; Version control interaction
(use-package magit
  :ensure t
  :after (evil ft-space-key)
  :commands magit-status
  :bind ("M-\\" . #'magit-status)
  :config
  (magit-auto-revert-mode -1)
  (add-hook 'git-commit-mode-hook 'evil-insert-state)
  (setq magit-completing-read-function #'magit-builtin-completing-read))

(use-package git-modes
  :ensure t
  :after magit)

(use-package forge
  :disabled t
  :ensure t
  :after magit)

;; Documentation systems
(use-package eldoc-box
  :disabled t
  :ensure t
  :after eglot
  :diminish
  :config
  (add-hook 'eglot-managed-mode-hook #'eldoc-box-hover-mode t))

(use-package info
  :after (evil ft-space-key)
  :commands (info info-mode)
  :config
  (evil-define-key '(normal motion) Info-mode-map
    (kbd "RET")   'Info-follow-nearest-node
    (kbd "l")     'evil-forward-char
    (kbd "M-l")   'Info-history-back
    (kbd "C-i")   'Info-next-reference
    (kbd "<tab>") 'Info-next-reference
    (kbd "M-j")   'scroll-up-command
    (kbd "M-k")   'scroll-down-command
    (kbd "C-j")   'end-of-buffer
    (kbd "C-k")   'beginning-of-buffer
    (kbd "SPC")   ft/space-map)
  (defun info-mode ()
    "Take the current buffer (hopefully) containing texinfo data, and launch it
in the `info' browser. Funny, that emacs doesn't have this by default. -ft"
    (interactive)
    (let ((fn (buffer-file-name)))
      (kill-buffer (current-buffer))
      (info fn))))

(use-package woman
  :preface
  (defvar woman-use-own-frame nil)
  (defvar woman-use-topic-at-point t))

(use-package help-mode
  :after ft-space-key
  :config
  (evil-define-key '(normal motion) help-mode-map
    (kbd "M-l")   'help-go-back
    (kbd "M-n")   'help-go-forward
    (kbd "SPC")   ft/space-map)
  (ft/bind-space (list help-mode-map)))

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 1)
  (which-key-mode))

(use-package rfc-mode
  :ensure t
  :commands (rfc-mode rfc-mode-browse)
  :config (setq rfc-mode-directory (expand-file-name "rfc-cache"
                                                     user-emacs-directory)))

(use-package srfi
  :ensure t)

;; Convenience
(use-package desktop
  :config
  (setq desktop-save 'if-exists)
  (setq desktop-dirname user-emacs-directory)
  (setq desktop-base-file-name "desktop")
  (setq desktop-base-lock-name "desktop.lock")
  (desktop-save-mode 1)
  (setq desktop-globals-to-save
        (append '((extended-command-history . 30)
                  (file-name-history        . 100)
                  (grep-history             . 30)
                  (compile-history          . 30)
                  (minibuffer-history       . 50)
                  (query-replace-history    . 60)
                  (read-expression-history  . 60)
                  (regexp-history           . 60)
                  (regexp-search-ring       . 20)
                  (search-ring              . 20)
                  (shell-command-history    . 50)
                  tags-file-name
                  register-alist))))

(use-package project
  :bind (:map ft/space-map
              ("P p" . #'project-switch-project)
              ("P f" . #'project-find-file))
  :config (setq project-vc-merge-submodules nil))

(use-package ft-projects
  :after project
  :bind (:map ft/space-map
              ("P P" . #'ft/project-from-candidates))
  :config
  (defun ft/is-project (dir subdir)
    (not (char-uppercase-p (string-to-char subdir))))
  (setq ft/projects/predicate 'ft/is-project)
  (setq ft/projects/sources '("~/src/prj"
                              "~/src/sys"
                              "~/src/test"
                              "~/src/ex"
                              "~/etc")))

(use-package savehist
  :preface
  (defvar savehist-autosave-interval nil)
  (defvar savehist-ignored-variables '(woman-topic-history))
  :config
  (savehist-mode 1))

;; File types
(use-package auctex
  :ensure t
  :commands (LaTeX-mode)
  :mode ("\\.tex\\'" . LaTeX-mode)
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (use-package latex
    :commands LaTeX-mode TeX-mode texinfo-mode
    :config
    (setq LaTeX-indent-level 4)
    (add-hook 'TeX-language-de-hook
              (lambda ()
                (ispell-change-dictionary "deutsch-alt")))
    (add-hook 'LaTeX-mode-hook #'LaTeX-preview-setup)
    (use-package preview
      :commands (LaTeX-preview-setup)
      :config (setq preview-gs-command (executable-find "gs")))
    (use-package reftex
      :config
      (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
      (setq reftex-plug-into-AUCTeX t))))

(use-package cc-mode
  :commands (c++-mode c-mode)
  :config
  (setq c-style-variables-are-local-p t)

  (defun ft/c-initialization-hook ()
    (define-key c-mode-base-map "\C-m" 'c-context-line-break))
  (add-hook 'c-initialization-hook 'ft/c-initialization-hook)

  (defconst xmms2-c-style
    '("k&r"
      (c-basic-offset             . 4)
      (tab-width                  . 4)
      (indent-tabs-mode           . t)
      (c-special-indent-hook      . smart-tab-indent-hook)
      (c-offsets-alist
       . ((arglist-close . c-lineup-arglist-intro-after-paren))))
    "Based on the `k&r' style. This is what xmms2 uses.
The basic offset is one tab which is supposed to be 4 characters wide.
They also want tabs for indentation and spaces for alignment, which
is achieved via the c-special-indent-hook `smart-tab-indent-hook'.")
  (c-add-style "xmms2" xmms2-c-style)

  (defconst zsh-bsd-c-style
    '("bsd"
      (c-basic-offset             . 4)
      (tab-width                  . 8)
      (indent-tabs-mode           . t))
    "Based on the `bsd' style. This is what zsh uses.
The basic offset is 4 spaces. 8 Spaces are replaced by a tab.")
  (c-add-style "zsh-bsd" zsh-bsd-c-style)

  (defconst ft/c-style
    '((c-basic-offset             . 4)
      (indent-tabs-mode           . nil)
      (c-comment-only-line-offset . 0)
      (c-hanging-braces-alist     . ((brace-list-open)
                                     (substatement-open after)))
      (c-hanging-colons-alist     . ((access-label after)
                                     (case-label after)
                                     (inher-intro)
                                     (label after)
                                     (member-init-intro before)))
      (c-cleanup-list             . (defun-close-semi
                                     empty-defun-braces
                                     scope-operator))
      (c-offsets-alist            . ((arglist-close         . c-lineup-arglist)
                                     (innamespace           . [0])
                                     (inextern-lang         . [0])
                                     (block-open            . 0)
                                     (case-label            . 0)
                                     (inexpr-class          . 0)
                                     (inline-open           . 0)
                                     (knr-argdecl-intro     . +)
                                     (label                 . 0)
                                     (statement-block-intro . +)
                                     (statement-cont        . +)
                                     (substatement-label    . 0)
                                     (substatement-open     . 0)))
      (c-echo-syntactic-information-p . t))
    "ft c style
Used for my pet projects; like taggit and cmc.
It indents by four spaces and is otherwise similar to the BSD style.")

  (defconst ft/c-eclipsish
    '((c-basic-offset             . 4)
      (tab-width                  . 4)
      (c-special-indent-hook      . smart-tab-indent-hook)
      (indent-tabs-mode           . t)
      (c-comment-only-line-offset . 0)
      (c-hanging-braces-alist     . ((brace-list-open)
                                     (substatement-open after)))
      (c-hanging-colons-alist     . ((access-label after)
                                     (case-label after)
                                     (inher-intro)
                                     (label after)
                                     (member-init-intro before)))
      (c-cleanup-list             . (defun-close-semi
                                     empty-defun-braces
                                     scope-operator))
      (c-offsets-alist            . ((arglist-close         . c-lineup-arglist)
                                     (block-open            . 0)
                                     (case-label            . 0)
                                     (inexpr-class          . 0)
                                     (inline-open           . 0)
                                     (inextern-lang         . [0])
                                     (innamespace           . [0])
                                     (knr-argdecl-intro     . +)
                                     (label                 . 0)
                                     (statement-block-intro . +)
                                     (statement-cont        . +)
                                     (substatement-label    . 0)
                                     (substatement-open     . 0)))
      (c-echo-syntactic-information-p . t))
    "C/C++ style similar to Eclipse's default")

  (c-add-style "ft" ft/c-style)
  (c-add-style "eclipsish" ft/c-eclipsish)

  (setq c-default-style '((java-mode . "java")
                          (awk-mode . "awk")
                          (other . "ft")))

  :config
  ;; `get-nonempty-context()' and `smart-tab-indent-hook()' implement
  ;; the `c-special-indent-hook' for xmms2's c-code indentation
  ;; style. All of this business comes from:
  ;; <http://bytes.inso.cc/wp/2009/01/07/dot-emacs-smarter-indentation-with-tabs-and-spaces/>
  ;; ...I just properly stuffed it into cc-mode's style system.
  (defun get-nonempty-context ()
    (let ((curr-context (car (c-guess-basic-syntax))))
      (if (or (eq (car curr-context) 'arglist-intro)
              (eq (car curr-context) 'arglist-cont)
              (eq (car curr-context) 'arglist-cont-nonempty)
              (eq (car curr-context) 'arglist-close))
          curr-context
        nil)))

  (defun smart-tab-indent-hook ()
    "Fixes indentation to pad with spaces in arglists - for xmms2 mode."
    (let ((nonempty-ctx (get-nonempty-context)))
      (if nonempty-ctx
          (let ((tabbed-columns (+ (point-at-bol)
                                   (/ (c-langelem-col nonempty-ctx t)
                                      tab-width)))
                (orig-column (current-column)))
            (tabify (point-at-bol) tabbed-columns)
            (untabify tabbed-columns (point-at-eol))
                                        ; editing tabs screws the pointer position
            (move-to-column orig-column)))))

  (defun ft/c-auto-set-style ()
    (let ((bfname (buffer-file-name)))
      (when (stringp bfname)
        (cond
         ((string-match "/src/sys/zsh/" bfname) (c-set-style "zsh-bsd"))
         ((string-match "/src/sys/xmms2/" bfname) (c-set-style "xmms2"))
         ))))

  (defun ft/c-mode-adjust ()
    (setq c-electric-pound-behavior '(alignleft))
    (ft/c-auto-set-style))

  (add-hook 'c-mode-common-hook 'ft/c-mode-adjust t))

(use-package clojure-mode
  :ensure t
  :commands (clojure-mode)
  :mode ("\\.clj\\'" . clojure-mode))

(use-package cmake-mode
  :ensure t
  :commands (cmake-mode)
  :mode
  ("CMakeLists\\.txt\\'" . cmake-mode)
  ("\\.cmake\\'" . cmake-mode))

(use-package debian-el
  :ensure t
  :mode ("\\.deb$" . deb-view-mode))

(use-package dpkg-dev-el
  :ensure t
  :mode
  ("/debian/*NEWS" . debian-changelog-mode)
  ("NEWS.Debian" . debian-changelog-mode)
  ("/debian/\\([[:lower:][:digit:]][[:lower:][:digit:].+-]+\\.\\)?changelog\\'" . debian-changelog-mode)
  ("changelog.Debian" . debian-changelog-mode)
  ("changelog.dch" . debian-changelog-mode)
  ("/debian/control\\'" . debian-control-mode)
  ("debian/.*copyright\\'" . debian-copyright-mode)
  ("\\`/usr/share/doc/.*/copyright" . debian-copyright-mode)
  ("debian/.*README.*Debian$" . readme-debian-mode)
  ("^/usr/share/doc/.*/README.*Debian.*$" . readme-debian-mode))

(use-package dts-mode
  :ensure t
  :mode
  ("\\.dtsi?\\'" . dts-mode)
  ("\\.dts_compiled\\'" . dts-mode)
  ("\\.dts\\..*\\'" . dts-mode))

(use-package elisp-mode
  :config
  (add-hook 'lisp-interaction-mode-hook
            (lambda () (setq mode-name "LispInt"))))

(use-package cperl-mode
  :commands cperl-mode
  :init
  (mapc (lambda (pair)
          (if (eq (cdr pair) 'perl-mode)
              (setcdr pair 'cperl-mode)))
        (append auto-mode-alist interpreter-mode-alist))

  (add-hook 'cperl-mode-hook
            (lambda ()
              (outline-minor-mode t)
              ;;(which-function-mode t)
              (eval-when-compile (require 'cperl-mode))
              (abbrev-mode -1)
              (setq
               indent-tabs-mode nil
               ;;cperl-hairy t
               cperl-indent-level 4
               cperl-brace-offset 0
               cperl-continued-brace-offset -4
               cperl-continued-statement-offset 4
               cperl-label-offset -4
               cperl-indent-parens-as-block t
               cperl-close-paren-offset -4
               cperl-invalid-face nil
               cperl-electric-parens nil
               cperl-electric-keywords t))))

(use-package gnuplot-mode
  :ensure t
  :commands (gnuplot-mode gnuplot-make-buffer)
  :mode ("\\.gp$" . gnuplot-mode))

(use-package groovy-mode
  :ensure t
  :commands (groovy-mode)
  :mode ("Jenkinsfile" . groovy-mode))

(use-package haskell-mode
  :ensure t
  :mode ("\\.l?hs\\'" . haskell-mode)
  :config
  (add-hook 'haskell-mode-hook #'haskell-indentation-mode)
  (add-hook 'haskell-mode-hook #'haskell-doc-mode))

(use-package idris-mode
  :ensure t
  :commands (idris-mode)
  :mode ("\\.l?idr\\'" . idris-mode))

(use-package ini-mode
  :ensure t
  :commands ini-mode
  :mode ("\\.ini\\'" . ini-mode))

(use-package intel-hex-mode
  :commands intel-hex-mode
  :ensure t
  :mode
  ("\\.ihex\\'" . intel-hex-mode)
  ("\\.hex\\'" . intel-hex-mode))

(use-package kconfig-mode
  :ensure t
  :commands kconfig-mode
  :config (add-hook 'kconfig-mode-hook
                    (lambda ()
                      (setq indent-tabs-mode t))))

(use-package ledger-mode
  :ensure t
  :commands ledger-mode
  :mode ("\\.ledger\\'" . ledger-mode)
  :preface
  (defvar ledger-clear-whole-transactions t)
  (defvar ledger-default-date-format "%Y-%m-%d")
  (defvar ledger-use-iso-dates t))

(use-package markdown-mode
  :commands markdown-mode
  :ensure t
  :mode (("\\.mdwn\\'" . markdown-mode)
         ("\\.md\\'" . markdown-mode)))

(use-package maxima
  :ensure t
  :commands maxima-mode
  :mode ("\\.mac" . maxima-mode))

(use-package octave
  :mode ("\\.m\\'" . octave-mode)
  :config (add-hook 'octave-mode-hook
                    (lambda ()
                      (setq octave-block-offset 4)
                      (setq octave-continuation-offset 8))))

(use-package protobuf-mode
  :ensure t
  :mode ("\\.proto\\'" . protobuf-mode)
  :commands (protobuf-mode))

(use-package scheme
  :after paredit
  :commands (scheme-mode)
  :mode ("\\.rkt\\'" . scheme-mode)
  :config
  (mapc (lambda (thing) (put (car thing)
                             'scheme-indent-function
                             (cdr thing)))
        '((with-test-bundle . 1)
          (with-fs-test-bundle . 1)
          (with-ellipsis . 1)
          (set-record-type-printer! . 1)
          (define-test . 1))))

(use-package sh-script
  :commands (shell-script-mode sh-script)
  :config
  (setq-default sh-indent-for-case-label 0)
  (setq-default sh-indent-for-case-alt '+))

(use-package t2t-mode
  :commands t2t-mode
  :mode ("\\.t2t$" . t2t-mode))

(use-package textile-mode
  :ensure t
  :mode ("\\.textile$" . textile-mode))

(use-package vivado-mode
  :commands vivado-mode
  :mode ("\\.xdc\\'" . vivado-mode))

(use-package yaml-mode
  :ensure t
  :commands yaml-mode
  :mode ("\\.yml\\'" . yaml-mode)
  :mode ("\\.yaml\\'" . yaml-mode))

;; Programming Language Support (more than just file-types)
(use-package clang-format
  :ensure t
  :commands (clang-format
             clang-format-buffer
             clang-format-region))

(use-package compile
  :commands compile recompile
  :config (setq compilation-scroll-output 'first-error))

(use-package eglot
  :commands (eglot eglot-ensure)
  :config
  (add-to-list 'eglot-ignored-server-capabilities :hoverProvider)
  (add-to-list 'eglot-ignored-server-capabilities :inlayHintProvider)
  (add-to-list 'eglot-server-programs
               `((c-mode c++-mode) .
                 ,(eglot-alternatives
                   '(("clangd" "--query-driver=/usr/bin/*gcc*")
                     "ccls")))))

(use-package geiser
  :ensure t
  :commands (run-guile
             run-geiser
             geiser-edit-symbol-at-point
             geiser-pop-symbol-stack)
  :config
  (evil-define-key 'normal scheme-mode-map
    (kbd "g t") #'geiser-edit-symbol-at-point
    (kbd "g b") #'geiser-pop-symbol-stack)
  (setq-default geiser-default-implementation 'guile)
  (add-hook 'geiser-repl-mode-hook
            (lambda ()
              (paredit-mode)
              (evil-define-key '(normal insert) geiser-repl-mode-map
                (kbd "RET") #'geiser-repl-maybe-send))))

(use-package geiser-chez
  :ensure t
  :after geiser)

(use-package geiser-guile
  :ensure t
  :after geiser
  :init (setenv "GUILE_AUTO_COMPILE" "0"))

(use-package geiser-racket
  :ensure t
  :after geiser)

(use-package paredit
  :ensure t
  :commands paredit-mode
  :diminish "pe"
  :init
  (mapc (lambda (hook)
          (add-hook hook (lambda ()
                           (paredit-mode 1))))
        '(clojure-mode
          emacs-lisp-mode-hook
          lisp-mode-hook
          lisp-interaction-mode-hook
          scheme-mode-hook))
  :config
  (evil-define-key '(insert replace normal) paredit-mode-map
    (kbd "C-j") #'eval-print-last-sexp)
  (evil-define-key '(insert replace) paredit-mode-map
    (kbd "C-c C-j") #'paredit-newline)
  ;; L prefix in normal mode for editing commands; "L" is actually taken in vi
  ;; like every key out there, but I never use it ever. And L makes a good
  ;; mnemonic with "Lisp".
  ;;
  (evil-define-key '(normal) paredit-mode-map
    ;; Barf and slurp:
    (kbd "Lb") #'paredit-forward-barf-sexp
    (kbd "Ls") #'paredit-forward-slurp-sexp
    (kbd "LB") #'paredit-backward-barf-sexp
    (kbd "LS") #'paredit-backward-slurp-sexp
    ;; Move S-expressions around in the list they are part of:
    (kbd "Lh") #'ft/move-sexp-backward
    (kbd "Ll") #'ft/move-sexp-forward
    (kbd "Lt") #'transpose-sexps
    ;; Splice, split and join:
    (kbd "L/") #'paredit-splice-sexp
    (kbd "L|") #'paredit-split-sexp
    (kbd "Lj") #'paredit-join-sexps
    ;; Killing words, S-expressions, etc:
    (kbd "LL") #'paredit-kill
    (kbd "Ld") #'kill-sexp
    (kbd "LD") #'backward-kill-sexp
    (kbd "Lk") #'paredit-forward-kill-word
    (kbd "LK") #'paredit-backward-kill-word
    ;; Miscellaneous editing commands:
    (kbd "Lr") #'paredit-raise-sexp
    ;; Movement:
    (kbd "M-f") #'ft/paredit-forward
    (kbd "M-b") #'paredit-backward
    (kbd "M-u") #'paredit-backward-up
    (kbd "M-d") #'paredit-forward-down
    (kbd "M-p") #'paredit-backward-down
    (kbd "M-n") #'ft/paredit-forward-up
    (kbd "M-F") #'forward-sexp
    (kbd "M-B") #'backward-sexp)
  (defun ft/paredit-forward (&optional arg)
    "Move forward an S-expression.

This is much like `paredit-forward', but also moves back and
forth once, if the point is not at an opening parenthesis. This
makes it work much better in evil mode and also makes it behave
more like the reverse functionality in `paredit-backward'.

If the first `paredit-forward' ends up right in front of a double
quote character, issue another `paredit-forward' to move to the
beginning of the next S-expression."
    (interactive "p")
    (paredit-forward arg)
    (when (= (char-after (point)) ?\")
      (paredit-forward))
    (unless (= (char-after (point)) ?\))
      (paredit-forward)
      (paredit-backward)))

  (defun ft/paredit-forward-up (&optional arg)
    "Like `ft/paredit-forward', but for `paredit-forward-up'."
    (interactive "p")
    (paredit-forward-up arg)
    (unless (= (char-after (point)) ?\))
      (paredit-forward)
      (paredit-backward)))

  (defun ft/move-sexp-backward (&optional arg)
    (interactive "p")
    (while (> arg 0)
      (transpose-sexps 1)
      (paredit-backward 2)
      (setq arg (- arg 1))))

  (defun ft/move-sexp-forward (&optional arg)
    (interactive "p")
    (while (> arg 0)
      (paredit-forward 1)
      (condition-case err
          (transpose-sexps 1)
        (scan-error
         (message "%s" (error-message-string err))))
      (paredit-backward 1)
      (setq arg (- arg 1)))))

(use-package rustic
  :ensure t
  :commands (rustic-mode))

;; Complete Applications (org, email)
(use-package org
  :after ft-space-key
  :commands (org-agenda
             org-capture
             org-mode
             org-store-link
             org-toggle-link-display)
  :bind (:map ft/space-map
              ("o c" . #'org-capture)
              ("o l" . #'org-store-link)
              ("o L" . #'org-toggle-link-display)
              ("o a" . #'org-agenda)
              ("t" . #'ft/org-todo-interactively))
  :mode ("\\.org\\'" . org-mode)
  :preface
  (require 'pcomplete)
  (require 'ido)
  (defun ft/org-todo-interactively ()
    "Like `org-todo', but uses a completing-read function to pick
the next state. Unlike it, this command does not take any
arguments."
    (interactive)
    (org-todo (funcall
               (cond ((fboundp 'ido-completing-read+) #'ido-completing-read+)
                     (t #'ido-completing-read))
               "Task State? "
               (pcomplete-uniquify-list
                (copy-sequence org-todo-keywords-1)))))
  (defvar org-capture-templates
    '(("t" "Add Task" entry
       (file+headline "~/org/inbox.org" "Task Inbox")
       "* NEW %?
:PROPERTIES:
:ID:       %(shell-command-to-string \"uuidgen\"):CREATED:  %U
:END:"
       :prepend t
       :empty-lines 0)
      ("n" "Add Note" entry
       (file+headline "~/org/new-notes.org" "Note Inbox")
       "* %?
:PROPERTIES:
:ID:       %(shell-command-to-string \"uuidgen\"):CREATED:  %U
:END:"
       :prepend t
       :empty-lines 0)
      ("w" "New Unsorted Web Link" entry
       (file+headline "~/org/bookmarks.org" "Unsorted Web Links")
       "* %(ft/org-capture-bookmark)
:PROPERTIES:
:ID: %(shell-command-to-string \"uuidgen\"):CREATED: %U
:TAGS: %^g
:END:\n\n"
       :empty-lines 0)
      ("f" "New Unsorted File Link" entry
       (file+headline "~/org/file-links.org" "Unsorted File Links")
       "* %a
:PROPERTIES:
:ID: %(shell-command-to-string \"uuidgen\"):CREATED: %U
:TAGS: %^g
:END:\n\n"
       :empty-lines 0)
      ("m" "New Unsorted Mailbox Link" entry
       (file+headline "~/org/email-links.org" "Unsorted Mailbox Links")
       "* %a
:PROPERTIES:
:SUBJECT: %:subject
:FROM: %:from
:TO: %:to
:DATE: %:date-timestamp-inactive
:GROUP: %:group
:MESSAGE-ID: %:message-id
:ID: %(shell-command-to-string \"uuidgen\"):CREATED: %U
:TAGS: %^g
:END:\n\n"
       :empty-lines 0)))
  :config
  (require 'org-agenda)
  (require 'org-capture)
  (require 'org-duration)
  (evil-define-key '(normal insert replace) org-mode-map
    (kbd "<backtab>") #'org-shifttab
    (kbd "M-h")       #'org-metaleft
    (kbd "M-H")       #'org-shiftmetaleft
    (kbd "M-j")       #'org-metadown
    (kbd "M-J")       #'org-shiftmetadown
    (kbd "M-k")       #'org-metaup
    (kbd "M-K")       #'org-shiftmetaup
    (kbd "M-l")       #'org-metaright
    (kbd "M-L")       #'org-shiftmetaright
    (kbd "TAB")       #'org-cycle)
  (add-to-list 'ft/private-space-key-callbacks
               `(org-mode . ,#'org-tree-to-indirect-buffer))
  (setq org-log-done t)
  (setq org-directory (expand-file-name "~/org"))
  (setq org-default-notes-file (concat org-directory "/scratch.org"))
  (setq org-agenda-files
        (mapcar (lambda (f) (expand-file-name (concat "~/org/" f ".org")))
                (list "inbox" "birthdays"
                      "software" "hardware"
                      "configuration")))
  (setq org-todo-keywords
        '((sequence
           "NEW"             ; Brand new, not categorised.
           "TODO"            ; Seen, categorised and marked to-be-done.
           "ACTIVE(!)"       ; Active work.
           "DEFERRED(!)"     ; Needs to be done, but not right now.
           "PAUSED(!)"       ; Work that is paused by me.
           "WAITING(!)"      ; Something that I can't control is pausing this.
           "DELEGATED(!)"    ; Someone else gets to do the honours. Should keep
           "|"               ; track on the tasks progress, though.
           "DONE(!)"         ; Done and dusted. Nothing left to do.
           "DUPLICATE(!)"    ; Took care of this in another entry.
           "CANCELED(!)")))  ; Not happening.

  (defun ft/org-capture-bookmark ()
    (let* ((url (substring-no-properties (gui-get-primary-selection)))
           (cmd (concat "curl -s "
                        (shell-quote-argument url)
                        " | hxnormalize -x"
                        " | hxselect -i -c title"
                        " | hxunent"))
           (title* (shell-command-to-string cmd))
           (title (replace-regexp-in-string "[ \n]+" " " title* nil t)))
      (concat title "\n" url)))

  ;; When following links to pdfs, use evince instead of docview.
  (add-to-list 'org-file-apps '("\\.pdf\\'" . "evince %s") t)
  (add-to-list 'org-file-apps '("\\.djvu\\'" . "djview %s") t)
  (add-to-list 'org-file-apps '("\\.\\(png\\|jpg\\|JPG\\)\\'" . "feh %s") t)

  ;; Fontify code blocks automatically.
  (setq org-src-fontify-natively t)

  ;; Don't add indentation to code blocks.
  (setq org-src-preserve-indentation t)

  ;; Do not indent org-mode paragraphs with outline node level
  (setq org-adapt-indentation nil)

  ;; eval&export shortcut
  (defun ft/org-eval-and-export ()
    "Evaluate all code blocks in the active org buffer and export
the file as PDF."
    (interactive)
    (let ((org-confirm-babel-evaluate nil)
          (org-export-use-babel nil))
      (org-babel-execute-buffer)
      (org-export-as-pdf 3)))

  (evil-add-hjkl-bindings org-agenda-mode-map 'emacs
    (kbd "C-e") #'evil-scroll-line-down
    (kbd "C-y") #'evil-scroll-line-up
    (kbd "C-d") #'evil-scroll-down
    (kbd "C-u") #'evil-scroll-up)

  (add-hook 'org-mode-hook
            (lambda ()
              ;; Strike-through is annoying with diff-stats in change logs.
              (when buffer-file-name
                (let ((name (file-name-base buffer-file-name))
                      (targets '("CHANGES" "ChangeLog")))
                  (when (member name targets)
                    (setq org-emphasis-alist '(("*" bold)
                                               ("/" italic)
                                               ("_" underline)
                                               ("=" org-verbatim verbatim)
                                               ("~" org-code verbatim)
                                               ("+" (:strike-through nil)))))))))
  (add-hook 'org-agenda-mode-hook (lambda () (hl-line-mode 1)))

  (w/function org-babel-do-load-languages
              (org-babel-do-load-languages 'org-babel-load-languages
                                           (mapcar (lambda (x)
                                                     (cons x t))
                                                   '(emacs-lisp
                                                     haskell
                                                     maxima
                                                     octave)))))

(use-package ox-textile
  :ensure t
  :after (org))

(use-package org-roam
  :disabled t
  :ensure t
  :after (org)
  :commands (org-roam-buffer-toggle
             org-roam-dailies-capture-today
             org-roam-graph
             org-roam-node-find
             org-roam-node-insert
             org-roam-setup)
  :bind (:map ft/space-map
              ("R l" . #'org-roam-buffer-toggle)
              ("R f" . #'org-roam-node-find)
              ("R i" . #'org-roam-node-insert))
  :config
  (setq org-roam-v2-ack t)
  (setq org-roam-directory (file-truename "~/org/roam"))
  (setq org-roam-db-location (expand-file-name "data.db" org-roam-directory))
  (org-roam-setup))

(when (file-exists-p "~/.guix-profile/share/emacs/site-lisp/mu4e")
  (use-package mu4e
    :load-path "~/.guix-profile/share/emacs/site-lisp/mu4e"
    :commands (mu4e)
    :config
    (setq mu4e-use-fancy-chars t)
    (setq mu4e-mu-binary "~/.guix-profile/bin/mu")
    (setq mu4e-sent-folder   "/Sent")
    (setq mu4e-drafts-folder "/Drafts")
    (setq mu4e-refile-folder "/Archive")
    (setq mu4e-trash-folder  "/Trash")))

(use-package ft-email
  :commands (email)
  :init (setq-default gnus-init-file
                      (expand-file-name "gnus.el" user-emacs-directory)))

(use-package bbdb
  :ensure t
  :after ft-email
  :preface (setq bbdb-file (expand-file-name "~/.sensdata/addressbook.bbdb"))
  :config
  (setq bbdb-phone-style nil)
  (setq bbdb-message-all-addresses t)
  (setq bbdb-complete-mail-allow-cycling t)
  (setq bbdb-completion-display-record nil)
  (setq bbdb-mua-update-interactive-p '(query . create))
  (setq bbdb-user-mail-address-re "\\(ft@\\(zsh\\|grml\\|bewatermyfriend\\.org\\)\\|terbeck@fh-aachen\\.de\\)")
  (bbdb-initialize 'message 'gnus))

(use-package calc
  :after ft-space-key
  :commands calc-mode calc calc-dispatch
  :config
  (setq calc-internal-prec 32)
  (setq calc-word-size 32)
  (setq calc-angle-mode 'rad)
  (setq calc-complex-mode 'polar)
  (setq calc-full-float-format '(eng 0))
  (setq calc-float-format '(eng 6))
  (setq calc-window-height (floor (/ (frame-text-lines) 2.75)))
  (ft/bind-space (list calc-mode-map)))

(use-package calendar
  :after ft-space-key
  :commands calendar
  :config
  (setq calendar-week-start-day 1)
  (setq calendar-intermonth-header
        (propertize "Wk" 'font-lock-face 'calendar-iso-week-header-face))
  (setq calendar-intermonth-text
        '(propertize
          (format "%2d"
                  (car
                   (calendar-iso-from-absolute
                    (calendar-absolute-from-gregorian (list month day year)))))
          'font-lock-face 'calendar-iso-week-face))
  (setq calendar-mode-line-format
        (list
         (calendar-mode-line-entry 'calendar-scroll-right
                                   "previous month" "<")
         (concat
          (calendar-mode-line-entry 'calendar-other-month
                                    "choose another month" nil "other")
          " / "
          (calendar-mode-line-entry 'calendar-goto-today
                                    "go to today's date" nil "today"))
         (calendar-mode-line-entry 'calendar-scroll-left
                                   "next month" ">")
         "" ""))
  (add-hook 'calendar-today-visible-hook 'calendar-mark-today)
  (ft/bind-space calendar-mode-map))

(use-package time
  :bind ("<f5>" . #'world-clock)
  :commands world-clock
  :init
  (setq world-clock-list
        '(("US/Hawaii" "Hawaii")
          ("America/Los_Angeles" "Los Angeles")
          ("America/Denver" "Denver")
          ("America/Winnipeg" "Winnipeg")
          ("America/New_York" "New York")
          ("America/Caracas" "Caracas")
          ("America/Manaus" "Manaus")
          ("America/Buenos_Aires" "Buenos Aires")
          ("America/Sao_Paulo" "São Paulo")
          ("Africa/Dakar" "Dakar")
          ("UTC" "UTC")
          ("Europe/London" "London")
          ("Europe/Berlin" "Berlin")
          ("Europe/Helsinki" "Helsinki")
          ("Europe/Istanbul" "Istanbul")
          ("Africa/Cairo" "Cairo")
          ("Asia/Baghdad" "Baghdad")
          ("Europe/Moscow" "Moscow")
          ("Asia/Tehran" "Tehran")
          ("Asia/Kolkata" "New Dehli")
          ("Asia/Novosibirsk" "Novosibirsk")
          ("Asia/Irkutsk" "Irkutsk")
          ("Asia/Hong_Kong" "Hong Kong")
          ("Asia/Shanghai" "Beijing")
          ("Australia/Perth" "Perth")
          ("Asia/Seoul" "Seoul")
          ("Asia/Yakutsk" "Yakutsk")
          ("Asia/Vladivostok" "Vladivostok")
          ("Asia/Tokyo" "Tokyo")
          ("Australia/Adelaide" "Adelaide")
          ("Australia/Melbourne" "Melbourne")
          ("Pacific/Auckland" "Auckland")))
  (setq world-clock-time-format "%R %5Z (UTC%z) - %d %3h - %A"))

(use-package dired
  :after ft-space-key
  :config (ft/bind-space (list dired-mode-map)))

(use-package eww
  :after (evil ft-space-key)
  :commands (eww eww-follow-link)
  :bind (:map ft/space-map
              ("w f" . #'eww-follow-link)
              ("w b" . #'eww)))

(use-package pdf-tools
  :ensure t
  :magic ("%PDF" . pdf-view-mode)
  :init
  (evil-set-initial-state 'pdf-view-mode 'emacs)
  (add-hook 'pdf-view-mode-hook
            (lambda ()
              (set (make-local-variable 'evil-emacs-state-cursor) (list nil))))
  :config
  (pdf-tools-install :no-query)
  (setq-default pdf-view-display-size 'fit-page)
  (evil-define-key 'normal pdf-view-mode-map
    (kbd "g")   #'pdf-view-first-page
    (kbd "G")   #'pdf-view-last-page
    (kbd "l")   #'image-forward-hscroll
    (kbd "h")   #'image-backward-hscroll
    (kbd "j")   #'pdf-view-next-line-or-next-page
    (kbd "k")   #'pdf-view-previous-line-or-previous-page
    (kbd "J")   #'pdf-view-next-page
    (kbd "K")   #'pdf-view-previous-page
    (kbd "q")   #'pdf-view-goto-page
    (kbd "C-r") #'pdf-view-midnight-minor-mode
    (kbd "u")   #'pdf-view-revert-buffer
    (kbd "a l") #'pdf-annot-list-annotations
    (kbd "a d") #'pdf-annot-delete
    (kbd "a a") #'pdf-annot-attachment-dired
    (kbd "a m") #'pdf-annot-add-markup-annotation
    (kbd "a t") #'pdf-annot-add-text-annotation
    (kbd "y")   #'pdf-view-kill-ring-save
    (kbd "i")   #'pdf-misc-display-metadata
    (kbd "s")   #'pdf-occur
    (kbd "b")   #'pdf-view-set-slice-from-bounding-box
    (kbd "r")   #'pdf-view-reset-slice))

(use-package ispell
  :commands (ispell-word ispell-buffer)
  :config
  (evil-global-set-key 'normal (kbd "g s") #'ft/spell-buffer)
  (evil-global-set-key 'normal (kbd "g S") #'ft/spelling-pick-dictionary)
  (use-package flyspell)
  (setq ispell-choices-win-default-height 5)
  (setq-default ispell-program-name "hunspell")

  (add-to-list 'ispell-local-dictionary-alist
               '("deutsch" "[[:alpha:]]" "[^[:alpha:]]" "[']"
                 t ("-d" "de_DE") nil utf-8))
  (add-to-list 'ispell-local-dictionary-alist
               '("british" "[[:alpha:]]" "[^[:alpha:]]" "[']"
                 t ("-d" "en_GB") nil utf-8))
  (add-to-list 'ispell-local-dictionary-alist
               '("american" "[[:alpha:]]" "[^[:alpha:]]" "[']"
                 t ("-d" "en_US") nil utf-8))

  (defvar ft/spelling-dictionaries
    (mapcar #'car ispell-local-dictionary-alist)
    "List of configured spelling dictionaries")

  (defun ft/spelling-pick-dictionary ()
    (interactive)
    (let* ((current (or ispell-local-dictionary ispell-dictionary))
           (dict (completing-read (format "Pick a dictionary (default: %s): "
                                          current)
                                  ft/spelling-dictionaries)))
      (ispell-change-dictionary dict))
    (when flyspell-mode
      (flyspell-delete-all-overlays)
      (flyspell-buffer)))

  (defun ft/spell-buffer ()
    (interactive)
    (unless flyspell-mode
      (let ((parents (derived-mode-parents major-mode)))
        (if (memq 'prog-mode parents)
            (flyspell-prog-mode)
          (flyspell-mode))))
    (flyspell-buffer))

  (setq ispell-dictionary "british"))

;; Miscellaneous Miscellany
(use-package browse-url
  :commands (browse-url
             browse-url-firefox
             browse-url-at-mouse
             browse-url-of-buffer
             browse-url-of-region
             browse-url-of-file
             browse-url-of-dired-file)
  :config
  (setq browse-url-browser-function #'browse-url-firefox)
  (setq browse-url-firefox-arguments '("-p" "generic")))

(use-package shr
  :config (setq shr-use-fonts nil))

(use-package ft-blog
  :commands (ftblog/new-entry))

;; Finish off by loading a site-specific file, if it exists.
(ft/load-elisp-file "init-post.el")
