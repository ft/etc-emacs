;; -*- lexical-binding: t; -*-

;; Module that helps setting up emacs frame in a mildly intelligent manner,
;; depending on the system that is running, according to `ft/host-system' as
;; defined by `ft-utilities', as well as the pixel density (pixels per milli-
;; metre) of the monitor that an emacs frame is created on.

(require 'ft-utilities)

(defvar ft/fallback-font-size 12
  "Font size to use in case a more suitable size could not be
figured out.")

(defvar ft/default-frame-alist
  '((horizontal-scroll-bars . nil)
    (vertical-scroll-bars . nil))
  "Partial value for `default-frame-alist'. Do not include a
`font' entry in here, as it will be added by the `ft-gui' module
instead.")

(defvar ft/gui-explicit-font-size nil
  "If an explicit font size was used previously, store it here. nil otherwise.")

(defun ft/font-format (system)
  "Return a default font format for a given SYSTEM.

The value returned is a string, usable by `format'. It should
contain exactly one \"%d\" that will be replaced by a font size
in `ft/generate-font-string'."
  (cond ((eq system 'ms-win) "Consolas-%d")
        ('default-gui-font   "DejaVu Sans Mono-%d")))

(defun ft/density-to-font-size (px)
  "Map font sizes to minimum pixel densities."
  (cond ((> px 8.0) 16)
        ((> px 7.0) 15)
        ((> px 6.0) 14)
        ((> px 4.5) 14)
        ((> px 3.5) 14)
        ((> px 2.0) 12)
        (t ft/fallback-font-size)))

(defun ft/monitor-to-pixel-density (m)
  "Return pixel density for a monitor parameter set M.

The parameter set can be retrieved by functions like
`frame-monitor-attributes' or `display-monitor-attributes'."
  (let* ((g (mapcar (lambda (x) (float (abs x)))
                    (cdddr (assq 'geometry m))))
         (s (mapcar (lambda (x) (if x (float x) nil))
                    (cdr (assq 'mm-size m))))
         (w (car s))
         (h (cadr s)))
    (if (and w h)
        (max (/ (car g) w)
             (/ (cadr g) h))
      0.0)))

(defun ft/max-pixel-density ()
  "Return the maximum pixel density available to the system."
  (apply #'max (mapcar #'ft/monitor-to-pixel-density
                       (display-monitor-attributes-list))))

(defun ft/pixel-density ()
  "Return pixel density of the dominating monitor to an emacs
frame."
  (ft/monitor-to-pixel-density (frame-monitor-attributes)))

(defun ft/generate-font-string (system &optional fsize)
  "Generate font string for SYSTEM to use in a `font' spec.

It takes into account the system that is running emacs and the
pixel density of the dominating monitor that the focused emacs
frame is visible on."
  (format (ft/font-format system)
          (or fsize (ft/density-to-font-size (ft/pixel-density)))))

(defun ft/frame-parameters (system &optional fsize)
  "Return frame parameter alist for SYSTEM."
  (cons (cons 'font (ft/generate-font-string system fsize))
        ft/default-frame-alist))

(defun ft/set-gui-buffer-parameters ()
  "Set GUI buffer handling parameters to something reasonable."
  ;; Put yanked stuff into primary buffer, if we're on X11 since this will
  ;; not work on M$-Windoze type systems — and terminal emacs doesn't give
  ;; a crap anyway.
  (w/variable select-enable-primary
              (setq select-enable-primary t))
  (w/variable select-enable-clipboard
              (setq select-enable-clipboard nil))
  (w/variable x-select-enable-clipboard-manager
              (setq x-select-enable-clipboard-manager nil))
  (w/variable x-stretch-cursor
              (setq x-stretch-cursor t)))

(defun ft/set-mouse-parameters ()
  "Set GUI mouse handling parameters to something reasonable."
  ;; Mouse avoidance mode is interesting, but I only liked the ‘banish’
  ;; mode it offers. Now after a while, I sometimes misclicked in another
  ;; application after switching to another workspace in my window manager.
  (w/function mouse-avoidance-mode
              (mouse-avoidance-mode 'none))

  ;; This is the default, but I wanted to remind myself that it exists.
  ;; Unfortuntely, it only makes the pointer invisible after *changing* a
  ;; buffer, and *not* if you merely move the cursor or scroll a buffer's
  ;; display. This is rather annoying for reading documentation within
  ;; emacs. Not great, but oh well.
  ;;
  ;; Update: There's https://github.com/jcs/xbanish which does pretty much
  ;; exactly what I want. Not in debian though... ...should I?
  (w/variable make-pointer-invisible
              (setq make-pointer-invisible t)))

(defun ft/update-frame-parameters (font-size)
  "Modify current frame's parameters according to system and monitor."
  (interactive "P")
  (setq ft/gui-explicit-font-size
        (cond ((integerp font-size) font-size)
              ((equal font-size '(4)) nil)
              (t ft/gui-explicit-font-size)))
  (let ((params (ft/frame-parameters (ft/host-system)
                                     ft/gui-explicit-font-size)))
    (modify-frame-parameters nil params)
    (when (called-interactively-p 'interactive)
      (message "Updating frame parameters (font: %s)"
               (cdr (assoc 'font params))))
    (setq default-frame-alist params)
    params))

(defvar ft/dark-themes
  '(modus-vivendi ft-dark)
  "Dark themes used by setup.")

(defvar ft/light-themes
  '(modus-operandi ft-light)
  "Light themes used by setup.")

(defun ft/load-theme (theme)
  (load-theme theme t nil))

(defun ft/load-colours (themes)
  (mapc #'disable-theme custom-enabled-themes)
  (mapc #'ft/load-theme themes))

(defun ft/dark-colours ()
  (interactive)
  (ft/load-colours ft/dark-themes))

(defun ft/light-colours ()
  (interactive)
  (ft/load-colours ft/light-themes))

(defun ft/toggle-colours ()
  (interactive)
  (if (memq (car ft/light-themes) custom-enabled-themes)
      (ft/dark-colours)
    (ft/light-colours)))

(defun ft/frame-creation-hook ()
  "ft-gui function to hook into `server-after-make-frame-hook'."
  (when (display-graphic-p)
    (ft/set-gui-buffer-parameters)
    (ft/set-mouse-parameters)
    (ft/update-frame-parameters nil)))

(defun ft/initialise-gui ()
  "Initialise the ft-gui module."
  (unless (daemonp)
    (setq default-frame-alist (ft/frame-parameters (ft/host-system))))
  (add-hook 'server-after-make-frame-hook #'ft/frame-creation-hook)
  (add-hook 'after-init-hook
            (lambda ()
              (w/function menu-bar-mode   (menu-bar-mode   -1))
              (w/function tool-bar-mode   (tool-bar-mode   -1))
              (w/function scroll-bar-mode (scroll-bar-mode -1))
              (unless (daemonp) (ft/frame-creation-hook)))))

(provide 'ft-gui)
