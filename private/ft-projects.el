;; -*- lexical-binding: t; -*-

;; project.el  is a  nice, lightweight  set of  operations upon  projects. Once
;; you've visited a project  with it, it will remember and  offer it again when
;; using ‘project-switch-project’.  When you've  never been  to a  project yet,
;; that's less convenient.
;;
;; This lets you specify a set of directories (‘ft/projects/sources’), in which
;; to find projects. Then using  ‘ft/project-from-candidates’ will ask the user
;; (via ‘completing-read’)  for a  project directory.  When selected,  this di-
;; rectory name  is passed to  ‘project-switch-project’, and if the  project is
;; opened by its  means, project.el will add the selected  directory to its set
;; of known projects.

(require 'project)

(defvar ft/projects/sources '("~/src")
  "List of directories, that contain project candidates.

Entries in this list are fed into ‘expand-file-name’, after which
they need to be an absolute name of a directory. Any entries that
do not match this criterium is ignored.")

(defvar ft/projects/predicate nil
  "Name of a predicate function to filter down candidates

If non-nil this needs to be a function of two arguments: The
source directory from ‘ft/projects/sources’ and the candidate
name from that directory.

If this function returns false, the candidate is discarded.")

(defun ft//project:is-candidate-source (name)
  (and (file-name-absolute-p name)
       (file-directory-p name)))

(defun ft//project:is-candidate (src name)
  (and (not (string= name "."))
       (not (string= name ".."))
       (file-directory-p (file-name-concat src name))
       (if ft/projects/predicate
           (funcall ft/projects/predicate src name)
         t)))

(defun ft//project:candidates (dir)
  (if (ft//project:is-candidate-source dir)
      (mapcar (lambda (f) (file-name-concat dir f))
              (seq-filter (lambda (f) (ft//project:is-candidate dir f))
                          (directory-files dir)))
    '()))

(defun ft/project-from-candidates ()
  "Open a project from ‘ft/projects/sources’."
  (interactive)
  (project-switch-project
   (completing-read "Open Project? "
                    (apply #'append (mapcar #'ft//project:candidates
                                            (mapcar #'expand-file-name
                                                    ft/projects/sources))))))

(provide 'ft-projects)
