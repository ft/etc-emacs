;; -*- lexical-binding: t; -*-

(setq-default inhibit-startup-message t)
(setq-default inhibit-startup-echo-area-message t)
(setq-default initial-scratch-message nil)

(setq-default eol-mnemonic-unix "x")
(setq-default eol-mnemonic-dos "d")
(setq-default eol-mnemonic-mac "m")
(setq-default eol-mnemonic-undecided "?")

(setq-default require-final-newline t)
(setq-default indent-tabs-mode nil)

(setq-default cursor-in-non-selected-windows nil)
(setq-default sentence-end-double-space nil)
(setq-default fill-column 79)

(setq-default custom-theme-directory (expand-file-name "themes" user-emacs-directory))

(prefer-coding-system 'utf-8)

(put 'narrow-to-defun 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(setq display-line-numbers-current-absolute t)
(setq display-line-numbers nil)

(defvar ft/line-number-style 'relative
  "Line number style activated by ‘ft/toggle-line-numbers’.")

(defun ft/pick-line-number-style ()
  "Select line number style used be ‘ft/toggle-line-numbers’."
  (interactive)
  (setq ft/line-number-style
        (intern (completing-read (format "Pick line number style (default: %s)"
                                         ft/line-number-style)
                                 '(absolute relative)))))

(defun ft/toggle-line-numbers (arg)
  "Toggle line-number display.

This uses ‘ft/line-number-style’ (absolute or relative). Use
‘ft/pick-line-number-style’ or call this command with a universal
argument to change this interactively."
  (interactive "P")
  (when arg
    (call-interactively #'ft/pick-line-number-style)
    ;; This makes sure the newly selected style is immediately activated,
    ;; irregardless of whether or not line number display was active prior.
    (setq display-line-numbers nil))
  (if display-line-numbers
      (setq display-line-numbers nil)
    (setq display-line-numbers
          (cond ((eq 'relative ft/line-number-style) 'relative)
                ((eq 'absolute ft/line-number-style) t)
                (t nil)))))

(global-set-key (kbd "<f3>")  #'ft/toggle-line-numbers)
(global-set-key (kbd "C-s")   #'isearch-forward-regexp)
(global-set-key (kbd "C-r")   #'isearch-backward-regexp)
(global-set-key (kbd "C-M-u") #'universal-argument)

(when (>= emacs-major-version 29)
  ;; In emacs 29, the default mode-line uses proportional fonts. This reverts
  ;; back to a monospaced font.
  (set-face-attribute 'mode-line-active nil :inherit 'mode-line)
  (set-face-attribute 'mode-line-inactive nil :inherit 'mode-line))

(cond ((boundp 'use-short-answers) (setq use-short-answers t))
      (t (fset 'yes-or-no-p 'y-or-n-p)))


(provide 'ft-core)
