;; -*- lexical-binding: t; -*-

;; I want two different colour schemes: A dark and a light one. I usually
;; prefer dark schemes, but when the environment around me is very bright, it's
;; hard to read dark displays.
;;
;; This used to be a complete theme. Now I'm using the modus themes and only
;; use these to configure things those don't cover. This is maybe a little much
;; overhead for just those few faces. But then again, the machinery is already
;; there.

(require 'cl-macs)
(require 'cus-face)

(defun ft/define-colour-scheme (light-scheme?)
  "Defines a colour scheme (or theme in emacs lingo).

If LIGHT-SCHEME? is non-nil, the function defines a colour scheme
with a light background. Otherwise a dark background colour is
used."
  ;; Since I'll forget how this works in a week or so, here's a short overview:
  ;;
  ;; Colours are defined as a list of pairs: First item is the minimum number
  ;; of colours to be available for the second item to be used:
  ;;
  ;;   '((256 . "darkblue")
  ;;     (  8 . "blue"))
  ;;
  ;; That means, in case 256 colours are available, the colour will be
  ;; "darkblue" otherwise it'll be "blue".
  ;;
  ;; Since this function is used to define two colour schemes, the local
  ;; `pick-colour' routine is used to make sure the right colours are used in
  ;; the right variant. For example:
  ;;
  ;;   (pick-colour '((256 . "darkblue")
  ;;                  (  8 . "blue"))
  ;;                '((256 . "darkred")
  ;;                  (  8 . "red")))
  ;;
  ;; That uses the blue-ish colours if the dark-theme variant is requested and
  ;; the red-ish colours are used if the light variant of the scheme is
  ;; requested.
  ;;
  ;; The logic below uses the tags `colour' and `attribs' to tell colours and
  ;; attribute lists apart. `fg' and `bg' create `colour' lists; `common'
  ;; creates `attribs' lists. `colours?' and `attribs?' are predicates to tell
  ;; them apart.
  ;;
  ;; The `attr' function puts the `colour' and `attribs' lists into a data
  ;; structure the custom-themes system of emacs expects.
  ;;
  ;; The `merge' function puts multiple colour definitions (foreground and
  ;; background usually) meant for one minimum colour requirement into one
  ;; alist where the key it the minimum number of colours the the value is a
  ;; corresponding plist. The `build-attr-case' function produces an emacs
  ;; custom-theme data structure from that alist. This is what the `colour'
  ;; lists get treated with.
  ;;
  ;; The `attribs' lists are concatenated into one big list. Its attributes are
  ;; stored in the `:default' clause of emacs's custom-theme data structure.
  ;;
  ;; That means that colours are picked up by minimum-colours available in a
  ;; display, while all other attributes are inserted into the face-spec's
  ;; `:default' clause, making them available with either colour that is used.
  ;;
  ;; Also: M-x rainbow-mode
  (cl-flet* ((ctsf (n &rest faces)
               (mapc (lambda (x) (make-face (car x))) faces)
               (apply 'custom-theme-set-faces n faces))
             (pick-colour (d l) (if light-scheme? l d))
             (common (&rest args) (cons 'attribs args))
             (colour (type c) (cons 'colour (mapcar (lambda (x) (list (car x)
                                                                      type
                                                                      (cdr x)))
                                                    c)))
             (fg (c) (colour :foreground c))
             (bg (c) (colour :background c))
             (colour? (x) (eq (car x) 'colour))
             (attribs? (x) (eq (car x) 'attribs))
             (add-to-alist (old key val)
               (let ((existed? nil))
                 (let ((rv (mapcar (lambda (x)
                                     (if (not (eq (car x) key))
                                         x
                                       (setq existed? t)
                                       (append x val)))
                                   old)))
                   (if existed?
                       rv
                     (append old (list (cons key val)))))))
             (merge (lst)
               (let ((rv nil))
                 (mapc (lambda (innerlist)
                         (mapc (lambda (x)
                                 (let ((num (car x))
                                       (rest (cdr x)))
                                   (setq rv (add-to-alist rv num rest))))
                               innerlist))
                       lst)
                 rv))
             (build-attr-case (lst)
               (mapcar (lambda (x)
                         (let ((num (car x))
                               (rest (cdr x)))
                           (cons (list '(class color)
                                       (list 'min-colors num))
                                 rest)))
                       (sort lst (lambda (x y) (> (car x)
                                                  (car y))))))
             (attr (&rest args)
               (let ((cs (build-attr-case
                          (merge (mapcar
                                  #'cdr (cl-remove-if-not #'colour?
                                                          args)))))
                     (as (apply #'append
                                (mapcar
                                 #'cdr (cl-remove-if-not #'attribs?
                                                         args)))))
                 (cond ((null as) cs)
                       ((null cs) (list (cons 'default as)))
                       (t (cons (cons 'default as) cs))))))

    (let* ((name (intern (concat "ft-" (if light-scheme? "light" "dark"))))
           ;; Special purpose colours
           (default-bg (pick-colour '((256 . "black")
                                      (  8 . "black"))
                                    '((256 . "#ffffff")
                                      (  8 . "white"))))
           (default (pick-colour '((256 . "#bebebe")
                                   (  8 . "white"))
                                 '((256 . "black")
                                   (  8 . "black"))))

           ;; Common highlighting palette
           (col-00 (pick-colour '((256 . "#ff5f00")
                                  (  8 . "red"))
                                '((256 . "#d70000")
                                  (  8 . "red"))))
           (col-01 (pick-colour '((256 . "#2faa27")
                                  (  8 . "green"))
                                '((256 . "#005239")
                                  (  8 . "green"))))
           (col-02 (pick-colour '((256 . "#8787d7")
                                  (  8 . "cyan"))
                                '((256 . "#15008a")
                                  (  8 . "magenta"))))
           (col-03 (pick-colour '((256 . "#de549e")
                                  (  8 . "magenta"))
                                '((256 . "#8a6500")
                                  (  8 . "magenta"))))
           (col-04 (pick-colour '((256 . "#dece54")
                                  (  8 . "red"))
                                '((256 . "#7a0061")
                                  (  8 . "red"))))
           (col-05 (pick-colour '((256 . "#cd5500")
                                  (  8 . "yellow"))
                                '((256 . "#523900")
                                  (  8 . "yellow"))))
           (col-06 (pick-colour '((256 . "#00af00")
                                  (  8 . "green"))
                                '((256 . "#8f00ff")
                                  (  8 . "green"))))
           (col-07 (pick-colour '((256 . "#884ad7")
                                  (  8 . "blue"))
                                '((256 . "#415200")
                                  (  8 . "blue"))))
           (col-08 (pick-colour '((256 . "#00cdcd")
                                  (  8 . "cyan"))
                                '((256 . "#006969")
                                  (  8 . "cyan"))))
           (col-09 (pick-colour '((256 . "#afff5f")
                                  (  8 . "magenta"))
                                '((256 . "#8a0030")
                                  (  8 . "magenta"))))
           (col-0a (pick-colour '((256 . "#ff5fd7")
                                  (  8 . "gray"))
                                '((256 . "#000087")
                                  (  8 . "gray"))))
           (col-0b (pick-colour '((256 . "#ffd700")
                                  (  8 . "yellow"))
                                '((256 . "#4222c1")
                                  (  8 . "blue"))))
           (col-0c (pick-colour '((256 . "#80a4fe")
                                  (  8 . "blue"))
                                '((256 . "#483d8b")
                                  (  8 . "blue"))))
           (col-0d (pick-colour '((256 . "#87cefa")
                                  (  8 . "blue"))
                                '((256 . "blue")
                                  (  8 . "blue"))))
           (col-0e (pick-colour '((256 . "#eeaa77")
                                  (  8 . "red"))
                                '((256 . "red")
                                  (  8 . "red"))))
           (col-0f (pick-colour '((256 . "#98fb98")
                                  (  8 . "green"))
                                '((256 . "#000080")
                                  (  8 . "blue"))))
           (col-11 (pick-colour '((256 . "magenta")
                                  (  8 . "magenta"))
                                '((256 . "#006969")
                                  (  8 . "cyan"))))
           (col-12 (pick-colour '((256 . "#787878")
                                  (  8 . "white"))
                                '((256 . "#181818")
                                  (  8 . "black"))))
           (col-13 (pick-colour '((256 . "#ff5858")
                                  (  8 . "white"))
                                '((256 . "#ee1258")
                                  (  8 . "white"))))
           (col-14 (pick-colour '((256 . "white")
                                  (  8 . "white"))
                                '((256 . "#440044")
                                  (  8 . "black"))))
           (col-15 (pick-colour '((256 . "#440044")
                                  (  8 . "blue"))
                                '((256 . "#eeaa77")
                                  (  8 . "red"))))

           ;; Colour-ramp-ish colour sets
           (ramp-00 (pick-colour '((256 . "#63889c")
                                   (  8 . "red"))
                                 '((256 . "#0000ff")
                                   (  8 . "blue"))))

           (ramp-01 (pick-colour '((256 . "#3e896d")
                                   (  8 . "red"))
                                 '((256 . "#784497")
                                   (  8 . "blue"))))

           (ramp-02 (pick-colour '((256 . "#b48a4b")
                                   (  8 . "red"))
                                 '((256 . "#97448d")
                                   (  8 . "blue"))))

           (ramp-03 (pick-colour '((256 . "#b2b44b")
                                   (  8 . "red"))
                                 '((256 . "#5557df")
                                   (  8 . "blue"))))

           (ramp-04 (pick-colour '((256 . "#ffaa44")
                                   (  8 . "red"))
                                 '((256 . "#2f6a54")
                                   (  8 . "blue"))))

           (ramp-05 (pick-colour '((256 . "#ffaa88")
                                   (  8 . "red"))
                                 '((256 . "#626a2f")
                                   (  8 . "blue"))))

           (ramp-06 (pick-colour '((256 . "#4bb481")
                                   (  8 . "red"))
                                 '((256 . "#a54a8b")
                                   (  8 . "blue"))))

           (ramp-07 (pick-colour '((256 . "#4bb2b4")
                                   (  8 . "red"))
                                 '((256 . "#8c3161")
                                   (  8 . "blue"))))

           ;; Static colour palette
           (col-white (pick-colour '((256 . "white")
                                     (  8 . "white"))
                                   '((256 . "white")
                                     (  8 . "white"))))
           (col-black (pick-colour '((256 . "black")
                                     (  8 . "black"))
                                   '((256 . "black")
                                     (  8 . "black"))))
           (col-red (pick-colour '((256 . "red")
                                   (  8 . "red"))
                                 '((256 . "red")
                                   (  8 . "red"))))
           (col-darkred (pick-colour '((256 . "#870000")
                                       (  8 . "red"))
                                     '((256 . "#870000")
                                       (  8 . "red"))))
           (col-green (pick-colour '((256 . "green")
                                     (  8 . "green"))
                                   '((256 . "darkgreen")
                                     (  8 . "green"))))
           (col-darkgreen (pick-colour '((256 . "darkgreen")
                                         (  8 . "darkgreen"))
                                       '((256 . "darkgreen")
                                         (  8 . "darkgreen"))))
           (col-blue (pick-colour '((256 . "blue")
                                    (  8 . "blue"))
                                  '((256 . "blue")
                                    (  8 . "blue"))))
           (col-darkblue (pick-colour '((256 . "#000080")
                                        (  8 . "blue"))
                                      '((256 . "#000080")
                                        (  8 . "blue"))))
           (col-lightblue (pick-colour '((256 . "#80a4fe")
                                         (  8 . "blue"))
                                       '((256 . "#80a4fe")
                                         (  8 . "blue"))))
           (col-yellow (pick-colour '((256 . "yellow")
                                      (  8 . "yellow"))
                                    '((256 . "yellow")
                                      (  8 . "yellow"))))
           (col-magenta (pick-colour '((256 . "magenta")
                                       (  8 . "magenta"))
                                     '((256 . "magenta")
                                       (  8 . "magenta"))))
           (col-cyan (pick-colour '((256 . "cyan")
                                    (  8 . "cyan"))
                                  '((256 . "cyan")
                                    (  8 . "cyan"))))
           (col-gray (pick-colour '((256 . "gray")
                                    (  8 . "gray"))
                                  '((256 . "gray")
                                    (  8 . "gray"))))
           (col-darkgray (pick-colour '((256 . "#1c1c1c")
                                        (  8 . "gray"))
                                      '((256 . "#1c1c1c")
                                        (  8 . "gray"))))
           (col-lightgray (pick-colour '((256 . "#d0d0d0")
                                         (  8 . "gray"))
                                       '((256 . "#d0d0d0")
                                         (  8 . "gray"))))
           (col-switchgray (pick-colour '((256 . "#1c1c1c")
                                          (  8 . "gray"))
                                        '((256 . "#d0d0d0")
                                          (  8 . "gray"))))
           (col-switchgray-dim (pick-colour '((256 . "#505050")
                                              (  8 . "white"))
                                            '((256 . "#b6b6b6")
                                              (  8 . "black"))))
           (col-switchblack (pick-colour '((256 . "white")
                                           (  8 . "white"))
                                         '((256 . "black")
                                           (  8 . "black"))))
           (col-switchgray-reverse (pick-colour '((256 . "#363636")
                                                  (  8 . "gray"))
                                                '((256 . "#c8c8c8")
                                                  (  8 . "gray"))))
           (col-switchgray-reverse-dim (pick-colour '((256 . "#1d1d1d")
                                                      (  8 . "gray"))
                                                    '((256 . "#e6e6e6")
                                                      (  8 . "gray")))))

      (ctsf
       name
       ;; Modus Overrides
       ;;
       ;; Some overrides are easy with the various modus-themes-*-overrides
       ;; variables in place, but some things are just easier to hammer over.
       ;; That's what we're doing here. For the variable based overrides see
       ;; the ‘custom’ stanza in ‘init.el’.
       `(default ,(attr (fg default)))
       `(header-line
         ,(attr (fg col-switchblack)
                (bg col-switchgray-reverse)
                (common :box (list :line-width 4
                                   :color (cdar col-switchgray-reverse)))))
       `(mode-line
         ,(attr (fg col-switchblack)
                (bg col-switchgray-reverse)
                (common :box (list :line-width 4
                                   :color (cdar col-switchgray-reverse)))))
       `(mode-line-inactive
         ,(attr (fg col-switchgray-dim)
                (bg col-switchgray-reverse-dim)
                (common :box (list :line-width 4
                                   :color (cdar col-switchgray-reverse-dim)))))
       `(font-lock-doc-face ,(attr (fg col-05)))
       `(gnus-button ,(attr (fg col-0b)))
       `(gnus-cite-1 ,(attr (fg col-01)))
       `(gnus-cite-2 ,(attr (fg col-02)))
       `(gnus-cite-3 ,(attr (fg col-03)))
       `(gnus-cite-4 ,(attr (fg col-04)))
       `(gnus-cite-5 ,(attr (fg col-05)))
       `(gnus-cite-6 ,(attr (fg col-06)))
       `(gnus-cite-7 ,(attr (fg col-07)))
       `(gnus-cite-8 ,(attr (fg col-08)))
       `(gnus-cite-9 ,(attr (fg col-09)))
       `(gnus-cite-10 ,(attr (fg col-0a)))
       `(gnus-cite-11 ,(attr (fg col-0b)))
       `(gnus-cite-attribution ,(attr (fg col-0f)))
       `(gnus-emphasis-bold-italic ,(attr (common :slant 'italic
                                                  :weight 'bold)))
       `(gnus-emphasis-italic ,(attr (common :slant 'italic)))
       `(gnus-emphasis-strikethru ,(attr (common :strike-through t)))
       `(gnus-emphasis-underline-bold-italic ,(attr (common :slant 'italic
                                                            :underline t
                                                            :weight 'bold)))
       `(gnus-emphasis-underline-italic ,(attr (common :slant 'italic
                                                       :underline t)))
       `(gnus-header-content ,(attr (fg col-0c)))
       `(gnus-header-from ,(attr (fg col-01)))
       `(gnus-header-name ,(attr (fg col-0c)))
       `(gnus-header-namesgroups ,(attr (fg col-00)))
       `(gnus-header-subject ,(attr (fg col-13)))
       `(gnus-signature ,(attr (fg col-red)))
       `(gnus-summary-high-read ,(attr (fg col-12)
                                       (common :weight 'bold)))
       `(gnus-summary-high-unread ,(attr (fg col-06)
                                         (common :weight 'bold)))
       `(gnus-summary-low-ancient ,(attr (fg col-07)))
       `(gnus-summary-low-read ,(attr (fg col-12)))
       `(gnus-summary-low-ticked ,(attr (fg col-02)))
       `(gnus-summary-low-unread ,(attr (fg col-06)))
       `(gnus-summary-normal-ancient ,(attr (fg col-07)))
       `(gnus-summary-normal-read ,(attr (fg col-12)))
       `(gnus-summary-normal-ticked ,(attr (fg col-00)))
       `(gnus-summary-normal-unread ,(attr (fg col-06)))

       `(mm-uu-extract ,(attr (fg col-switchgray)
                              (bg col-01)
                              (common :extend t)))
       `(whitespace-tab ,(attr (bg col-switchgray-reverse-dim)))

       ;; Private faces below.
       `(ft/display-time-world-face ,(attr (common :height 1.1)))
       `(ft/evil-modeline-insert-face
         ,(attr (bg col-01)
                (fg default-bg)
                (common :inherit 'ft/evil-modeline-base-face)))
       `(ft/evil-modeline-motion-face
         ,(attr (bg col-02)
                (fg default-bg)
                (common :inherit 'ft/evil-modeline-base-face)))
       `(ft/evil-modeline-replace-face
         ,(attr (bg col-00)
                (fg default-bg)
                (common :inherit 'ft/evil-modeline-base-face)))
       `(ft/face-modeline-buffername ,(attr (fg col-0f)))
       `(ft/face-modeline-position ,(attr (fg col-08)))
       `(ft/face-modeline-modified ,(attr (fg col-white) (bg col-red)))
       `(ft/face-modeline-meta)
       `(ft/face-modeline-file-mode ,(attr (fg col-white) (bg col-blue)))
       `(ft/gnus-article-face-size ,(attr (common :height 1.05)))
       `(ft/gnus-default ,(attr (fg default)
                                (common :slant 'normal
                                        :weight 'normal)))
       `(ft/gnus-diff-add ,(attr (fg col-green)))
       `(ft/gnus-diff-remove ,(attr (fg col-red)))
       `(ft/gnus-diff-stat-file ,(attr (fg col-07)))
       `(ft/gnus-diff-stat-num ,(attr (fg col-04)))
       `(ft/gnus-diff-header ,(attr (fg col-0c)))
       `(ft/gnus-diff-index ,(attr (fg col-0c)))
       `(ft/gnus-diff-hunk ,(attr (fg col-02)))
       `(ft/gnus-commit-message  ,(attr (fg default)))
       `(ft/gnus-face-group-empty ,(attr (fg col-12)))
       `(ft/gnus-face-group-many ,(attr (fg col-08)))
       `(ft/gnus-face-group-some ,(attr (fg col-06)))
       `(ft/gnus-face-group-ticked ,(attr (fg col-00)))
       `(ft/gnus-face-summary-date ,(attr (fg col-08)))
       `(ft/gnus-face-summary-flags ,(attr (fg col-13)))
       `(ft/gnus-face-summary-name ,(attr (fg col-05)))
       `(ft/gnus-face-summary-threadartn ,(attr (fg col-08)))
       `(ft/gnus-face-topic-line ,(attr (fg col-0e)))
       `(ft/gnus-header-to ,(attr (fg col-0b)))
       `(ft/gnus-header-cc ,(attr (fg col-03)))))))

(provide 'ft-colours)
