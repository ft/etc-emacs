;; -*- lexical-binding: t; -*-

(require 'files)

(defun ft/display ()
  "Return the process's $DISPLAY environment variable."
  (getenv "DISPLAY"))

(defun ft/host-name ()
  "Reflect the name of the host emacs is running on. Extract the
only the host-name if the `system-name' function returns a fqdn."
  (let ((name (system-name)))
    (substring name 0 (string-match "\\." name))))

(defun ft/host-system ()
  "Return the display-system the current emacs instance is running in. Can
be one of `console', `ms-win', `x-win', `mac', `dos' or `unknown'."
  (cond
   ((eq window-system 'x) 'x-win)
   ((eq window-system nil) 'console)
   ((memq window-system '(win32 w32)) 'ms-win)
   ((eq window-system 'ns) 'mac)
   ((eq window-system 'pc) 'dos)
   (t 'unknown)))

(defun ft/term ()
  "Return the process's $TERM environment variable."
  (or (getenv "TERM") "dumb"))

(defun ft/load-elisp-file (file)
  "Load additional setup files, if they exist."
  (let ((fullname (expand-file-name file user-emacs-directory)))
    (if (file-exists-p fullname)
      (load fullname)
      (message (format "Could not load `%s': File does not exist."
                       fullname)))))

(defun ft/current-position ()
  "Echo the current buffer position in the minibuffer."
  (interactive)
  (let* ((c (+ 1 (current-column)))
         (C (save-excursion
              (move-end-of-line nil)
              (current-column)))
         (l (let ((a (point-min))
                  (p (point))
                  (m (point-max)))
              (cond ((> m p) (count-lines a (+ 1 p)))
                    (t (+ 1 (count-lines a m))))))
         (L (count-lines (point-min) (point-max)))
         (s (point))
         (S (point-max))
         (% (* 100.0 (/ (float s)
                        (float S)))))
    (message "%s" `(column ,c of ,C — line ,l of ,L — pos ,s of ,S
                           — ,(format "%2.1f%%" %)))))

(defun ft/mode-to-hook (mode)
  "Return the symbol for the generic hook to a given `mode'."
  (intern (concat (symbol-name mode) "-hook")))

(defun revert-all-buffers ()
  "Refreshes all open buffers from their respective files"
  (interactive)
  (let* ((list (buffer-list))
         (buffer (car list)))
    (while buffer
      (let ((buffile (buffer-file-name buffer)))
        (when buffile
          (cond ((file-exists-p buffile)
                 (set-buffer buffer)
                 (revert-buffer t t t))
                (t (message "File seems gone: %s" buffile)))))
      (setq list (cdr list))
      (setq buffer (car list))))
  (message "Refreshing open files"))

(defun derived-mode-parents (mode)
  (and mode
       (cons mode (derived-mode-parents
                   (get mode 'derived-mode-parent)))))

(defmacro ft/register-to-hooks (fct list-of-hooks)
  "Register a function `fct' to a number of hooks listed in `list-of-hooks'."
  `(mapc (lambda (hook)
           (add-hook hook ,fct))
         ,list-of-hooks))

(defmacro w/variable (name &rest code)
  "Evaluate CODE if NAME is a bound variable."
  `(when (boundp (quote ,name))
     ,@code))

(defmacro w/feature (name &rest code)
  "Evaluate CODE if NAME is an emacs feature that is available."
  `(when (featurep (quote ,name))
     ,@code))

(defmacro w/function (name &rest code)
  "Evaluate CODE if NAME is a bound function."
  `(when (fboundp (quote ,name))
     ,@code))

(provide 'ft-utilities)
