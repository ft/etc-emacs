;; -*- lexical-binding: t; -*-

;; Sometimes I'd like to be able to edit the value of a variable, using emacs
;; as the editor. Zsh has vared after all. In order to to that, we'll create a
;; form like this: (setq variable-name variable-value), putting point onto the
;; variable-value.
;;
;; With that, there are two things:
;;
;;  ‘variable-edit’
;;      Edits the variable right here in the current buffer.
;;  ‘variable-edit-in-scratch’
;;      Switches to the *scratch* buffer, moves to its end, and edits
;;      the variable value in there.
;;
;; List values get pretty-printed. If paredit is available, the pretty printed
;; value gets reindented as well.

(require 'help-fns)
(require 'pp)

(defun variable-edit--variables ()
  ;; This is from the (interactive …) thing in ‘describe-variable’.
  (let ((v (variable-at-point))
        (enable-recursive-minibuffers t)
        (orig-buffer (current-buffer))
        val)
    (setq val (completing-read
               (if (symbolp v)
                   (format
                    "Edit variable (default %s): " v)
                 "Edit variable: ")
               #'help--symbol-completion-table
               (lambda (vv)
                 (with-current-buffer orig-buffer
                   (or (get vv 'variable-documentation)
                       (and (boundp vv) (not (keywordp vv))))))
               t nil nil
               (if (symbolp v) (symbol-name v))))
    (list (if (equal val "")
              v (intern val)))))

(defun variable-edit (&optional word)
  "Insert setq for a variable at point."
  (interactive (variable-edit--variables))
  (let ((value (and (boundp word)
                    (symbol-value word))))
    (forward-line)
    (insert "(setq ")
    (insert (symbol-name word))
    (if (and value (listp value))
        (progn (newline)
               (insert "'"))
      (insert " "))
    (save-excursion
      (insert (pp-to-string value))
      (when (bolp)
        (backward-delete-char 1 nil))
      (insert ")")
      (newline))
    (when (and (listp value)
               (fboundp 'paredit-reindent-defun))
      (paredit-reindent-defun))))

(defun variable-edit-in-scratch (&optional word)
  "Insert setq for a variable at at the end of *scratch* buffer."
  (interactive (variable-edit--variables))
  (switch-to-buffer (get-buffer-create "*scratch*"))
  (goto-char (point-max))
  (variable-edit word))

(provide 'variable-edit)
