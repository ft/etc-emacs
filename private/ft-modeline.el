;; -*- lexical-binding: t; -*-

(defface ft/face-modeline-modified
  nil
  "Face for modeline modification make-up."
  :group 'configuration/ft)

(defface ft/face-modeline-file-mode
  nil
  "Face for modeline file-mode make-up."
  :group 'configuration/ft)

(defface ft/face-modeline-buffername
  nil
  "Face for modeline buffer name make-up."
  :group 'configuration/ft)

(defface ft/face-modeline-position
  nil
  "Face for modeline buffer position make-up."
  :group 'configuration/ft)

(defvar ft/evil-modeline-delimiter-follows-state t
  "If nil, evil-modeline delimiters use the additional
ft/evil-delimiter-modeline-face face instead of following the
make-up of the state tag.")

(defface ft/evil-modeline-base-face
  nil
  "Basic face for evil modeline make-up."
  :group 'configuration/ft)

(defface ft/evil-modeline-normal-face
  '((t (:inherit ft/evil-modeline-base-face)))
  "Evil normal-state modeline make-up."
  :group 'configuration/ft)

(defface ft/evil-modeline-insert-face
  '((t (:inherit ft/evil-modeline-base-face)))
  "Evil insert-state modeline make-up."
  :group 'configuration/ft)

(defface ft/evil-modeline-replace-face
  '((t (:inherit ft/evil-modeline-base-face)))
  "Evil replace-state modeline make-up."
  :group 'configuration/ft)

(defface ft/evil-modeline-visual-face
  '((t (:inherit ft/evil-modeline-base-face)))
  "Evil visual-state modeline make-up."
  :group 'configuration/ft)

(defface ft/evil-modeline-operator-face
  '((t (:inherit ft/evil-modeline-base-face)))
  "Evil operator-state modeline make-up."
  :group 'configuration/ft)

(defface ft/evil-modeline-motion-face
  '((t (:inherit ft/evil-modeline-base-face)))
  "Evil motion-state modeline make-up."
  :group 'configuration/ft)

(defface ft/evil-modeline-emacs-face
  '((t (:inherit ft/evil-modeline-base-face)))
  "Evil emacs-state modeline make-up."
  :group 'configuration/ft)

(defface ft/evil-delimiter-modeline-face
  '((t (:inherit ft/evil-modeline-base-face)))
  "Evil delimiter modeline make-up."
  :group 'configuration/ft)

(defun ft//make-evil-modeline-face-name ()
  (intern (concat "ft/evil-modeline-" (symbol-name evil-state) "-face")))

(defun ft/evil-modeline-face ()
  (let ((face (ft//make-evil-modeline-face-name)))
    (and (facep face) face)))

(defun ft/evil-mode-line-tag ()
  (pcase (if (evil-visual-state-p) evil-visual-selection 'other)
    ('block (concat evil-mode-line-tag "B"))
    ('line  (concat evil-mode-line-tag "L"))
    (_      evil-mode-line-tag)))

(defun ft/evil-mode-line ()
  (let* ((tag (ft/evil-mode-line-tag))
         (tag-face (ft/evil-modeline-face))
         (del-face (if ft/evil-modeline-delimiter-follows-state
                       tag-face
                     'ft/evil-delimiter-modeline-face)))
    `((:propertize ("‹")  face ,del-face)
      (:propertize (,tag) face ,tag-face)
      (:propertize ("›")  face ,del-face))))

(setq-default
 mode-line-buffer-identification
 '((:propertize
    "%b"
    face ft/face-modeline-buffername
    help-echo
    "Buffer name
mouse-1: Previous buffer
mouse-3: Next buffer"
    mouse-face mode-line-highlight
    local-map (keymap
               (header-line keymap
                            (mouse-3 . mode-line-next-buffer)
                            (down-mouse-3 . ignore)
                            (mouse-1 . mode-line-previous-buffer)
                            (down-mouse-1 . ignore))
               (mode-line keymap
                          (mouse-3 . mode-line-next-buffer)
                          (mouse-1 . mode-line-previous-buffer))))))

(setq-default
 mode-line-modified
 `((:propertize
    "%1*"
    face ft/face-modeline-modified
    help-echo mode-line-read-only-help-echo
    local-map (make-mode-line-mouse-map mouse-1 ,#'mode-line-toggle-read-only)
    mouse-face mode-line-highlight)
   (:propertize
    "%1+"
    face ft/face-modeline-modified
    help-echo mode-line-modified-help-echo
    local-map (make-mode-line-mouse-map mouse-1 ,#'mode-line-toggle-modified)
    mouse-face mode-line-highlight)))

(setq-default
 mode-line-mule-info
 `(""
   (current-input-method
    (:propertize
     ("" current-input-method-title)
     help-echo (concat
                "Current input method: "
                current-input-method
                "
mouse-2: Disable input method
mouse-3: Describe current input method")
     local-map mode-line-input-method-map
     face ft/face-modeline-file-mode
     mouse-face mode-line-highlight))
   (:propertize
    "%z"
    help-echo mode-line-mule-info-help-echo
    face ft/face-modeline-file-mode
    mouse-face mode-line-highlight
    local-map mode-line-coding-system-map)
   (:eval (mode-line-eol-desc))))

(setq-default
 mode-line-client
 `(""
   (:propertize ("" (:eval (if (frame-parameter nil 'client) "@" "-")))
                face ft/face-modeline-meta
                help-echo ,(purecopy "emacsclient frame"))))

(setq-default mode-line-front-space " ")
(setq-default mode-line-delim-space " ")
(setq-default mode-line-end-spaces "")

(defvar ft/header-line
  '(mode-line-front-space
    (:propertize ("[" mode-line-modified "]")
                 face ft/face-modeline-modified)
    (:propertize ("[" mode-line-mule-info "]")
                 face ft/face-modeline-file-mode)
    mode-line-delim-space
    "« " mode-line-buffer-identification " »"
    (vc-mode ("" mode-line-delim-space "‹" vc-mode " ›")))
  "Header line format for ft-modeline.")

(defvar ft/mode-line-prefix
  '("%e"
    mode-line-front-space
    (:eval (ft/evil-mode-line))
    mode-line-delim-space
    (:propertize ("[" mode-line-remote mode-line-client "]")
                 face ft/face-modeline-meta))
  "Mode line format for ft-modeline.")

(defvar ft/mode-line-main
  '((:propertize "%p" face ft/face-modeline-position)
    " " mode-line-modes mode-line-misc-info mode-line-end-spaces)
  "Mode line format for ft-modeline.")

(defun ft/line:pre ()
  (setq evil-mode-line-format nil))

(defun ft/line:post ()
  (with-current-buffer "*Messages*"
    (setq mode-line-format (default-value 'mode-line-format))))

(defun ft/line:mode-and-header ()
  (ft/line:pre)
  (setq-default header-line-format (cons "" ft/header-line))
  (setq-default mode-line-format (append ft/mode-line-prefix
                                         (list mode-line-delim-space)
                                         ft/mode-line-main))
  (ft/line:post))

(defun ft/line:mode-just ()
  (ft/line:pre)
  (setq-default header-line-format nil)
  (setq-default mode-line-format (append ft/mode-line-prefix
                                         (cdr ft/header-line)
                                         (list mode-line-delim-space)
                                         ft/mode-line-main))
  (ft/line:post))

;; Some modes don't need a header line that displays mostly file based infor-
;; mation. It's not terrible if it's there, but if we can safe the space, why
;; wouldn't we?
(dolist (hook '(help-mode-hook
                magit-status-mode-hook))
  (add-hook hook (lambda () (setq header-line-format nil))))

(provide 'ft-modeline)
