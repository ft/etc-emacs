;; -*- lexical-binding: t; -*-

;; This uses <http://funtoo.org/Keychain> in order to determine environment
;; variables that configure connections to key agents like ssh-agent.

(require 'elisp-mode)

(defvar keychain-command
  "keychain --quiet --eval"
  "Name of the `keychain' script; possibly the absolute path,
including the appropriate options.")

(defvar keychain-buffer
  "*Output from keychain*"
  "Name of the buffer to send the output from keychain to.")

(defun keychain-transform ()
  (when (looking-at "[a-zA-Z0-9_]*=[^;]*; export")
    (insert "(setenv \"")
    (search-forward "=")
    (delete-char -1)
    (insert "\" \"")
    (search-forward ";")
    (delete-char -1)
    (insert "\")"))

  (let ((beg (point)))
    (move-end-of-line 1)
    (delete-region beg (point)))

  (when (looking-at "^$")
    (let ((beg (point)))
      (forward-line 1)
      (delete-region beg (point))))

  (forward-line 1))

(defun keychain-update ()
  (interactive)
  (let ((buf (with-current-buffer (get-buffer-create keychain-buffer)
               (setq buffer-read-only nil)
               (goto-char (point-min))
               (erase-buffer)
               (current-buffer))))
    (call-process-shell-command keychain-command nil buf t)
    (with-current-buffer buf
      (goto-char (point-min))
      (while (zerop (keychain-transform))
        t)
      (unless (string-equal major-mode "lisp-interaction-mode")
        (lisp-interaction-mode))
      (eval-buffer))))

(provide 'keychain-environment)
