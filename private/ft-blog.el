;; -*- lexical-binding: t; -*-

(defvar ftblog/postings-directory "~/src/web/blog/p"
  "Posting directory for the blog we're handling.")

(defun ftblog/new-entry ()
  "Opens a new blog posting file in `ftblog/postings-directory'.

The naming convention is to create a subdirectory which represents the current
year. In that directory there are files called xxx.mdwn, where \"xxx\" are a
number of hex digits (0-f). The initial width is three digits, thus the first
file name would be \"000.mdwn\".

Three digits make room for (16^3 - 1) == 4095 blog entries per year. If that
is somehow not enough, this function will use four-digit names, starting at
\"1000.mdwn\" (five-digits after that ran out of space, etc).

After doing this, you probably want to run `ftblog/insert-meta'."
  (interactive)
  (let* ((year (format-time-string "%Y"))
         (dir (concat ftblog/postings-directory "/" year))
         (first (concat dir "/000.mdwn")))

    (if (not (file-accessible-directory-p ftblog/postings-directory))
        (message (format "Couldn't access `%s'. Giving up!"
                         ftblog/postings-directory))
      (if (not (file-accessible-directory-p dir))
          (progn
            (mkdir dir)
            (find-file first))
        (if (not (file-readable-p first))
            (find-file first)
          (let* ((num (string-to-number
                       (substring
                        (car
                         ;; Yes, this assumes that - when sorted - the last
                         ;; directory entry is the last posting of the current
                         ;; year.
                         (last
                          (directory-files dir nil ".*\\.mdwn$" nil)))
                        0 -5)
                       16))
                 (file (format "%s/%03x.mdwn" dir (+ num 1))))
            (find-file file)))))))

(defun ftblog/meta-insert ()
  "Inserts some ikiwiki [[!meta... boilerplate applicable for new postings."
  (interactive)
  (goto-char (point-min))
  (insert "[[!meta title=\"\"]]\n")
  (insert "[[!meta author=\"ft\"]]\n")
  (insert (concat "[[!meta date=\"" (format-time-string "%c") "\"]]\n"))
  (insert "[[!pagetemplate template=\"postpage.tmpl\"]]\n")
  (insert "[[!tag ]]\n\n")
  (goto-char (point-min))
  (search-forward "\""))

(defun ftblog/updated-insert ()
  "Insert an `updated' meta entry for the current date. This does *not*
remove or change old updated-meta-headers."
  (interactive)
  (goto-char (point-min))
  (re-search-forward "meta date")
  (goto-char (+ (point-at-eol) 1))
  (insert (concat "[[!meta updated=\"" (format-time-string "%c") "\"]]\n"))
  (goto-char (point-at-bol))
  (re-search-forward "\n\n" (point-max) t))

(provide 'ft-blog)
