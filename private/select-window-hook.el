;; -*- lexical-binding: t; -*-

;; Adds a hook that is called when select-window focuses a window.

(require 'nadvice)

(defvar select-window-hook nil
  "List of functions to run in the select-window-hook.")

(defun select-window-hook-fnc (window &optional norecord)
  (unless norecord
    (run-hooks 'select-window-hook)))

(defun insinuate-select-window-hook ()
  (advice-add 'select-window :after #'select-window-hook-fnc))

(provide 'select-window-hook)
