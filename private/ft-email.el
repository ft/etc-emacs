;; -*- lexical-binding: t; -*-

(require 'epg)
(require 'gnus)
(require 'ldap)
(require 'mml2015)
(require 'mml1991)
(require 'mm-decode)
(require 'use-package)

(setq epg-user-id "E1D988BD")
(setq mml1991-use 'epg)
(setq mml2015-use 'epg)
(setq eudc-server-hotlist '(("" . bbdb)))

(defun ft/eudc-expand-inline (&optional replace)
  (interactive)
  (let ((completing-read-function #'completing-read))
    (eudc-expand-inline replace)))

(setq mm-decrypt-option 'known)
(setq mm-verify-option 'known)
(setq mm-discouraged-alternatives '("text/html" "text/richtext"))
(setq mm-automatic-display (remove "text/html" mm-automatic-display))

(defun email ()
  (interactive)
  (gnus))

(provide 'ft-email)
