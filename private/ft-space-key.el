;; -*- lexical-binding: t; -*-

;; Implement a keymap that is bound to the space key. This is useful for my
;; preference of using evil mode to get an easily accessible private keymap my
;; pressing the SPC key. It makes sense to bind this in evil-normal-state-map
;; and evil-motion-state-map. The ft/bind-space function can be used enable
;; this feature in keymaps.
;;
;;   (ft/bind-space (list evil-normal-state-map
;;                        evil-motion-state-map))
;;
;; It's obviously not as useful with insert state, though it makes sense to
;; bind ft/space-map command to another key in the global keymap as well. For
;; instance:
;;
;;   (define-key global-map (kbd "C-c C-o") ft/space-map)
;;
;; It may also be useful to bind this in other major-mode's keymap that work as
;; a non-editor user-interface like magit's modes and gnus' modes:
;;
;;   (ft/bind-space calendar-mode-map)
;;
;; Inside the ft/space-map keymap, the space key is again special to dispatch
;; to a major-mode specific operation. The operation can be specified in two
;; ways:
;;
;;   - By adding a (major-mode . command) or (major-mode . keymap) specifica-
;;     tion to ‘ft/private-space-key-callbacks’
;;   - By defining a keymap ‘ft/space-map/MAJOR-MODE-HERE’ keymap, such as
;;     ‘ft/space-map/org-mode’
;;
;; In case of the keymap choices here, ‘set-transient-map’ is used to tempora-
;; rily enable the specified keymap for the next keypress. If no such major-
;; mode specific configuration is done "SPC SPC" will echo a message to the
;; minibuffer.

(defvar ft/space-map (make-sparse-keymap)
  "Private keymap to be used with evil mode, and wherever else
applicable.")

(defvar ft/private-space-key-callbacks nil
  "major-mode to callback mapping for the space key in `ft/space-map'.

The target of the mapping can be either a callable command or
another keymap that will be enabled for the next keypress.")

(defun ft/private-space-key ()
  "Binding for the space key in ‘ft/space-map’.

This dispatches to a major-mode specific operation, if specified.
See the package's documentation for details."
  (interactive)
  (let ((spec (assq major-mode ft/private-space-key-callbacks)))
    (cond
     ((functionp (cdr spec)) (call-interactively (cdr spec)))
     ((keymapp (cdr spec)) (set-transient-map (cdr spec)))
     (t (let ((lmap (intern (concat "ft/space-map/" (symbol-name major-mode)))))
          (cond ((boundp lmap) (set-transient-map (symbol-value lmap)))
                (t (message "space: No specification for %s" major-mode))))))))

(defun ft/bind-space (where)
  "Enable space-key binding in a given major-mode or list of major
modes' keymaps."
  (dolist (keymap (if (keymapp where) (list where) where))
    (define-key keymap (kbd "SPC") ft/space-map)))

(define-key ft/space-map (kbd "SPC") #'ft/private-space-key)

(provide 'ft-space-key)
