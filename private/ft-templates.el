;; -*- lexical-binding: t; -*-

(require 'cl-lib)
(require 'project)

;; Allow the definition of template functions. These take three arguments:
;;
;;   - The name of the function.
;;   - The name of a symbol (see below).
;;   - A fallback string.
;;
;; The symbol can be either bound as a function or as a variable. If both are
;; bound, the function wins. A function value is called and must produce a
;; string that is inserted. A string value is inserted as well. Non-string
;; values should not be used in templates directly put to implement other
;; template helpers.
;;
;; If neither value is bound, the fallback string is inserted.
;;
;; This can be useful to set project-specific information in ‘.dir-locals.el’.

(defmacro ft/define-template (name symbol fallback)
  `(defun ,name ()
     (or (and (fboundp ',symbol) (,symbol))
         (and (boundp ',symbol) ,symbol)
         ,fallback)))

;; -------- Utility Functions -------- ;;

(defun ft//tempel:pad-string (s n)
  (let ((s* (make-string n ?\s)))
    (store-substring s* 0 s)))

(defun ft//strip-a-subdir (file subdirs)
  (catch 'done
    (dolist (sd subdirs)
      (when (string-prefix-p sd file)
        (throw 'done (string-remove-prefix sd file))))
    file))

(defun ft//file-parts (file root subdirs strip-ext)
  (let* ((ext (file-name-extension file))
         (str (ft//strip-a-subdir
               (replace-regexp-in-string "^/*" ""
                                         (string-remove-prefix root file))
               subdirs))
         (data (file-name-split (if strip-ext
                                    (substring str 0
                                               (- (length str)
                                                  (length ext)
                                                  1))
                                  str))))
    (if (string-empty-p (car data))
        (cdr data)
      data)))

(defun ft//tempel:these-file-parts (&optional subdirs strip-ext)
  (let ((p (project-current)))
    (ft//file-parts (expand-file-name (buffer-file-name))
                    (expand-file-name (project-root p))
                    subdirs strip-ext)))

(defun ft//tempel:get:c-function ()
  (save-excursion
    (let ((start (point)))
      (re-search-forward "(")
      (string-split (buffer-substring-no-properties start (1- (point)))
                    "[ \n\t\r]+" t nil))))

(defun ft//tempel:get:c-arguments ()
  (save-excursion
    (re-search-forward "(")
    (let ((start (point)))
      (re-search-forward ")")
      (mapcar (lambda (s)
                (string-split (replace-regexp-in-string "[\][*]" "" s)
                              "[ \n\t\r]+" t nil))
              (string-split (buffer-substring-no-properties start (1- (point)))
                            "," t nil)))))

(defun ft//tempel:c-filter-stuff (lst)
  (cl-remove-if (lambda (e)
                  (member e '("const"
                              "extern"
                              "inline"
                              "static")))
                lst))

(defun ft//tempel:c-arg-name (lst)
  (car (last lst)))

(defun ft//tempel:c-arg-type (lst)
  (string-join (ft//tempel:c-filter-stuff (reverse (cdr (reverse lst))))
               " "))

;; -------- Usable Template Items Below -------- ;;

(ft/define-template ft/tempel:author  meta:project-author  "PROJECT-AUTHOR")
(ft/define-template ft/tempel:licence meta:project-licence "LICENCE")
(ft/define-template ft/tempel:code    meta:project-code
                    '("src" "app" "prg" "include" "scheme"))

(defun ft/tempel:year ()
  "Produce the current year."
  (number-to-string (decoded-time-year (decode-time (current-time)))))

(defun ft/tempel:just-file (&optional sansext)
  "Produce the buffer file name without directory parts."
  (funcall (if sansext #'file-name-base #'file-name-nondirectory)
           (or (buffer-file-name) (buffer-name))))

(defun ft/tempel:random-hex (&optional n)
  "Return a string of (kind of) random hex digits.

Under the hood, this uses ‘md5’ and ‘current-time’. So this is
not even close to cryptographically strong randomness. But that's
not the point. The purpose is to add a little bit of randomness
to things like c-guards, to avoid accidental reuse of macros.

For stuff like that, this is strong enough.

Since this uses ‘md5’, the maximum number if digits is 32. If the
optional parameter exceeds that number it is clipped to that."
  (let ((s (md5 (format "%s" (current-time)))))
    (if n
        (substring s 0 (min 32 n))
      s)))

(defun ft/tempel:scheme-module ()
  "Produce a (guile module hierarchy) for a file buffer."
  (format "%s" (ft//tempel:these-file-parts (ft/tempel:code) t)))

(defun ft/tempel:haskell-module ()
  "Produce a Haskell.Module.Hierarchy for a file buffer."
  (string-join (ft//tempel:these-file-parts (ft/tempel:code) t) "."))

(defun ft/tempel:c-guard (&optional prefix)
  "Produce a C/C++ include guard macro name."
  (let ((f (string-join (ft//tempel:these-file-parts (ft/tempel:code)) "_")))
    (concat (if prefix prefix "")
            (upcase (replace-regexp-in-string "[-_./]" "_" f))
            "_"
            (ft/tempel:random-hex 8))))

(defun ft/tempel:c-return-type ()
  "Produce return type of C/C++ function after point."
  (ft//tempel:c-arg-type (ft//tempel:get:c-function)))

(defun ft/tempel:c-function-name ()
  "Produce name of C/C++ function after point."
  (car (last (ft//tempel:get:c-function))))

(defun ft/tempel:c-doxy-params ()
  "Produce a parameter table for C/C++ function after point."
  (let ((data (ft//tempel:get:c-arguments))
        (separator " *"))
    (if (or (not data)
            (equal data '(("void"))))
        separator
      (let* ((prefix " * @param  ")
             (todo "TODO: Briefly describe: ")
             (args (apply #'append (mapcar #'last data)))
             (longest (apply #'max (mapcar #'length args))))
        (string-join
         (cons separator
               (append
                (mapcar (lambda (a)
                          (format "%s%s  %s%s"
                                  prefix
                                  (ft//tempel:pad-string (ft//tempel:c-arg-name a)
                                                         longest)
                                  todo
                                  (ft//tempel:c-arg-type a)))
                        data)
                (list separator)))
         "\n")))))

(provide 'ft-templates)
