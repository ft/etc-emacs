;; -*- lexical-binding: t; -*-

;; Usually, when reading source code I don't quite care so much about the way
;; whitespace is laid out. When editing, though, I do care deeply. So with evil
;; mode, it might be reasonable to turn whitespace-mode on/off whether or not
;; we're in insert/normal state.
;;
;; In addition, it might be useful to leave the whitespace-mode state (on/off)
;; alone. That way I can force it to a desired state.
;;
;; In order for this to work, use ‘evil-whitespace-mode’ instead of the usual
;; ‘whitespace-mode’ in hooks that enable the mode. This way, the system can
;; track the major modes that whitespace-mode was enabled in, in order to guard
;; against undesired whitespace effects in some modes, like ‘package-menu-mode’.

(require 'whitespace)

(defvar ft/evil-whitespace-auto t
  "Global parameter, controlling the automatic nature of evil-whitespace.")

(defvar ft/evil-whitespace-modes '()
  "List of major modes evil-whitespace was activated in")

(defun evil-whitespace-toggle ()
  (interactive)
  (setq ft/evil-whitespace-auto (not ft/evil-whitespace-auto)))

(defun evil-whitespace-mode (&optional arg)
  "This is a wrapper around whitespace mode, that records the major mode
it was called in."
  ;;(message "ewm: (arg %s) (major-mode %s)" arg major-mode)
  (unless (member major-mode ft/evil-whitespace-modes)
    (add-to-list 'ft/evil-whitespace-modes major-mode))
  (whitespace-mode arg))

(defun evil-whitespace-maybe (&optional arg)
  "This is a wrapper around whitespace mode, that respects the values
of ‘ft/evil-whitespace-auto’ and ‘ft/evil-whitespace-modes’."
  (and ft/evil-whitespace-auto
       (member major-mode ft/evil-whitespace-modes)
       (whitespace-mode arg)))

(defun evil-whitespace-edit ()
  "A hook for the evil insert state. Turns whitespace-mode on."
  ;; Yeah, nil turns it on.
  (evil-whitespace-maybe nil))

(defun evil-whitespace-read ()
  "A hook for the evil normal state. Turns whitespace-mode off."
  ;; And anything that's not nil or toggle will turn it off.
  (evil-whitespace-maybe -1))

(defun evil-whitespace-setup ()
  "Setup the default evil state hooks for operation of the package."
  (add-hook 'evil-insert-state-entry-hook #'evil-whitespace-edit)
  (add-hook 'evil-normal-state-entry-hook #'evil-whitespace-read))

(provide 'evil-whitespace)
