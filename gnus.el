(message "Firing up gnus...")

(require 'gnus-sum)
(require 'ft-space-key)

(defun ft/kill-gnus ()
  (when (gnus-buffer-live-p "*Group*")
    (let ((gnus-expert-user t))
      (gnus-group-exit))))

(add-to-list
 'ft/private-space-key-callbacks
 `(gnus-group-mode . ,(lambda (&optional ALL)
                        (interactive)
                        (if (memq 'gnus-topic-mode minor-mode-list)
                            (call-interactively #'gnus-topic-select-group)
                          (call-interactively #'gnus-group-select-group)))))

;; A place for score files, I would edit manually.
(setq ft/gnus-score-dir "~/etc/mailnews/gnus/")

;; Here can I find my `authinfo' file.
(setq ft/gnus-auth-dir "~/etc/mailnews/")

;; `ft/gnus-var-dir' is used for our newsrc.
(setq ft/gnus-var-dir "~/var/lib/gnus/")

;; And a place for files that are used for adaptive scoring.
(setq ft/gnus-adaptive-score-dir "~/etc/mailnews/gnus-adaptive/")

;; Force tmux title to "mail/news".
(cond ((and (string-equal (ft/host-system) "console")
            (string-match "^screen" (ft/term)))
       (send-string-to-terminal "\ekmail/news\e\\")))

;; Tell me stuff.
(setq gnus-verbose 9)

;; I got local news and imap servers, so I don't need agent.
(setq gnus-agent nil)

;; Sometimes, it makes sense to set this to 'all.
(setq gnus-read-active-file 'some)

;; Like `gnus-demon-scan-news', but don't scan everything.
;; My `mairix' group is at level 5, so scan at level 4.
(defun ft/gnus-demon-scan-news ()
  (let ((win (current-window-configuration)))
    (unwind-protect
      (save-window-excursion
        (save-excursion
          (when (gnus-alive-p)
            (save-excursion
              (set-buffer gnus-group-buffer)
              (gnus-group-get-new-news 4)))))
      (set-window-configuration win))))

;; Check all sources for new article every 2 minutes.
;; If gnus-read-active-file is 'all, this may be annoying.
(defun ft/gnus-scan-news ()
  (ft/gnus-demon-scan-news)
  (ft/gnus-group-go-home)
  (hl-line-highlight))

(gnus-demon-add-handler 'ft/gnus-scan-news 2 t)

;; So, what's a large group?
(setq gnus-large-newsgroup 1000)

;; No thanks, no marks files.
(setq nntp-marks-is-evil t)

;; Don't use ~/.newsrc but our own.
(setq gnus-startup-file (concat ft/gnus-var-dir "newsrc.localhost"))

;; This should speed up things...
(setq gnus-check-bogus-newsgroups nil)
(setq gnus-check-new-newsgroups nil)
(setq gnus-save-newsrc-file nil)
(setq gc-cons-threshold 3500000)

;; This should improve thread building when using 'A T'.
;; It's slow, though. Let's turn it off.
;(setq gnus-fetch-old-headers 'invisible)

;; I'm using `gcc-self' to save sent messages into my own folders. Mark them as
;; read right at the time of saving.
(setq gnus-gcc-mark-as-read t)

;; Set Message-Id: to something that doesn't use the invalid domain
;; name of my laptop.
(defun message-make-message-id ()
    (concat "<" (message-unique-id) "@ft.bewatermyfriend.org>"))

;; Use message mode for mails, with all the gnus burp.
(setq mail-user-agent 'gnus-user-agent)

;; Send mail via the system's sendmail program.
(setq send-mail-function 'sendmail-send-it)

;; This is not exactly a gnus internal, but an emacs internal. But
;; I'm only changing it for gnus so far, so it makes sense to put
;; it here.
(setq message-log-max 512)

;; Move to or home position; see `gnus-keys-group.el'.
(add-hook 'gnus-started-hook 'ft/gnus-group-go-home)

;; Only ask for a destination file once when saving multiple articles:
(setq gnus-prompt-before-saving t)

;; If I'm replying to something that was signed, sign my mail. If it was
;; encrypted, encrypt my mail. You get the drift.
(setq gnus-message-replysign t)
(setq gnus-message-replyencrypt t)

;; Create clickable regions, which cover the results of
;; decrypt/verify attempts.
(setq gnus-buttonized-mime-types '("multipart/encrypted" "multipart/signed"))

;; When sending encrypted messages, I'd like to save an unencrypted copy.
;; That's okay, since I save messages on my local machine, that's otherwise
;; entirely encrypted. If that wouldn't be the case, it would make sense to
;; configure gpg to always encrypt with my own key as well.
;;
;; Otherwise, it's kind of hard to read my own replies...
(add-hook 'gnus-gcc-pre-body-encode-hook
          (lambda () (cond ((ft/is-secured-message-p)
                            (mml-unsecure-message)
                            (ft/set-message-sent-securely t))
                           (t (ft/set-message-sent-securely nil)))))

(defun ft/set-message-sent-securely (p)
  (message-remove-header "x-message-sent-securely")
  (message-add-header (concat "X-Message-Sent-Securely: "
                              (if p "yes" "no"))))

(defun ft/is-secured-message-p ()
  (save-excursion (message-goto-body)
                  (looking-at "^<#secure .*mode=\\(sign\\)?encrypt")))

(defun exit-emacs-with-gnus ()
  "Save and exit gnus before exiting Emacs."
  (interactive)
  (if (fboundp 'gnus-group-exit) (gnus-group-exit))
  (save-buffers-kill-emacs))

(defun ft/gnus-buffer-last-line ()
  "Move to the last line in a gnus buffer."
  (interactive)
  (end-of-buffer)
  (previous-line)
  (gnus-goto-colon))

(defun ft/gnus-article-keys ()
  (evil-normal-state)
  (define-key
    evil-normal-state-local-map (kbd "TAB") 'forward-button)
  (define-key
    evil-normal-state-local-map (kbd "M-a") 'ft/gnus-select-summary-window)
  (define-key
    evil-normal-state-local-map (kbd "q") 'gnus-summary-expand-window)
  (define-key gnus-article-mode-map (kbd "SPC") ft/space-map))

(add-hook 'gnus-article-mode-hook 'ft/gnus-article-keys)
(add-hook 'gnus-article-mode-hook #'ft/gnus-article-fonts)
(add-hook 'gnus-article-mode-hook
          (lambda () (setq header-line-format
                           '(" « " mode-line-buffer-identification " »"))))

(defun ft/gnus-group-topic-hide-or-show-high (hide)
  (goto-char (point-min))
  (while (progn
           (when (gnus-group-topic-p)
             (let ((lvl (gnus-group-topic-level)))
               (when (>= lvl 2)
                 (cond (hide (gnus-topic-hide-topic))
                       (t (gnus-topic-show-topic))))))
           (next-line)
           (< (point) (point-max))))
  (ft/gnus-group-go-home))

(defun ft/gnus-group-topic-hide-high-levels ()
  "Hide topics that are on an indentation level >= 2."
  (interactive)
  (ft/gnus-group-topic-hide-or-show-high t))

(defun ft/gnus-group-topic-unhide-high-levels ()
  "Show topics that are on an indentation level >= 2."
  (interactive)
  (ft/gnus-group-topic-hide-or-show-high nil))

(defun ft/gnus-group-topic-up ()
  "Move up a topic, even to parents."
  (interactive)
  (previous-line)
  (while (not (gnus-group-topic-p)) (previous-line))
  (gnus-goto-colon))

(defun ft/gnus-group-topic-down ()
  "Move down a topic, even to parents."
  (interactive)
  (next-line)
  (while (not (gnus-group-topic-p)) (next-line))
  (gnus-goto-colon))

(defun ft/gnus-group-go-home ()
  (with-current-buffer gnus-group-buffer
                       (gnus-group-goto-group "nnimap+localmail:Inbox" t nil)
                       (gnus-goto-colon)))

(defun ft/gnus-group-first-unread ()
  (interactive)
  (beginning-of-buffer)
  (gnus-group-next-unread-group 1))

(defun ft/gnus-group-last-unread ()
  (interactive)
  (end-of-buffer)
  (gnus-group-prev-unread-group 1))

(defun ft/gnus-group-go-home-and-get ()
  (interactive)
  (ft/gnus-group-go-home)
  (gnus-group-get-new-news 4))

(defun ft/gnus-group-next-line ()
  (interactive)
  (next-line)
  (gnus-goto-colon))

(defun ft/gnus-group-previous-line ()
  (interactive)
  (previous-line)
  (gnus-goto-colon))

(defun ft/gnus-group-pick-spam ()
  (interactive)
  (gnus-group-jump-to-group "nnimap+localmail:Spam")
  (gnus-topic-select-group))

(defun ft/gnus-group-pick-spam-unsure ()
  (interactive)
  (gnus-group-jump-to-group "nnimap+localmail:Spam-unsure")
  (gnus-topic-select-group))

(defun ft/gnus-group-keys ()
  ;; I never want something to read the 1st article of a group.
  ;; This needs to be in the topic map, because topic mode redefines
  ;; the space key. This also requires us to add ft/gnus-group-keys() to
  ;; the gnus-topic-mode-hook below.
  (define-key gnus-group-mode-map (kbd "SPC") ft/space-map)
  (define-key gnus-topic-mode-map (kbd "SPC") ft/space-map)
  (define-key gnus-group-mode-map (kbd "x") #'gnus-group-select-group)
  (define-key gnus-topic-mode-map (kbd "x") #'gnus-topic-select-group)
  (unbind-key (kbd "C-w") gnus-group-mode-map)
  (unbind-key (kbd "C-w") gnus-topic-mode-map)

  ;; Movement by topics. Including folding them.
  (define-key gnus-group-mode-map (kbd "<up>") 'ft/gnus-group-topic-up)
  (define-key gnus-group-mode-map (kbd "<down>") 'ft/gnus-group-topic-down)
  (define-key gnus-group-mode-map (kbd "<left>") 'gnus-topic-hide-topic)
  (define-key gnus-group-mode-map (kbd "<right>") 'gnus-topic-show-topic)

  ;; In group mode, home and end should jump up and down.
  (define-key gnus-group-mode-map (kbd "<home>") 'beginning-of-buffer)
  (define-key gnus-group-mode-map (kbd "<end>") 'ft/gnus-buffer-last-line)

  (define-key gnus-group-mode-map (kbd "M-f") 'ft/gnus-group-first-unread)
  (define-key gnus-group-mode-map (kbd "M-l") 'ft/gnus-group-last-unread)

  ;; And j/k need to behave like they do in vi.
  (define-key gnus-group-mode-map (kbd "j") 'gnus-group-next-group)
  (define-key gnus-group-mode-map (kbd "k") 'gnus-group-prev-group)
  (define-key gnus-group-mode-map (kbd "J") 'ft/gnus-group-next-line)
  (define-key gnus-group-mode-map (kbd "K") 'ft/gnus-group-previous-line)

  ;; Seriously, tab doing indention? I don't think so...
  (define-key gnus-topic-mode-map (kbd "TAB") 'gnus-group-next-unread-group)
  (define-key gnus-topic-mode-map (kbd "<tab>") 'gnus-group-next-unread-group)
  (define-key gnus-topic-mode-map (kbd "<backtab>") 'gnus-group-prev-unread-group)

  ;; `g' can do some more work.
  (define-key gnus-group-mode-map (kbd "g") 'ft/gnus-group-go-home-and-get)

  ;; 'k' does nothing in groupmode, currently. 'j' does, so
  ;; let's save its action to the user keymap.
  (define-key gnus-group-mode-map (kbd "vj") 'gnus-group-jump-to-group)
  (define-key gnus-group-mode-map (kbd "vg") 'gnus-group-get-new-news)
  (define-key gnus-group-mode-map (kbd "vG") 'ft/gnus-getmail)
  (define-key gnus-group-mode-map (kbd "vss") 'ft/gnus-group-pick-spam)
  (define-key gnus-group-mode-map (kbd "vsu") 'ft/gnus-group-pick-spam-unsure)

  ;; Hide/Unhide high-level topics.
  (define-key gnus-group-mode-map (kbd "vW") 'ft/gnus-group-topic-unhide-high-levels)
  (define-key gnus-group-mode-map (kbd "vw") 'ft/gnus-group-topic-hide-high-levels)

  ;; `Gbm' and `Gbs' don't tell me much. `vm' and `vM' are just mairix in
  ;; my key table (prefix `v').
  (define-key gnus-group-mode-map (kbd "vm") 'nnmairix-search)
  (define-key gnus-group-mode-map (kbd "vM") 'nnmairix-widget-search)

  ;; I'm using gnus in its own emacs session.
  ;; So quit that session when gnus exits.
  ;;(define-key gnus-group-mode-map (kbd "q") 'exit-emacs-with-gnus)
  ;; And in case I do just want to exit gnus:
  ;;(define-key gnus-group-mode-map (kbd "vQ") 'gnus-group-exit)

  ;; oh, and I'm continously pressing C-l. I don't want the screen
  ;; to flip/flop around...
  (define-key gnus-group-mode-map (kbd "C-l") 'redraw-display))

(add-hook 'gnus-topic-mode-hook 'ft/gnus-group-keys)

;; Message mode configuration. The message composition mode of choice...

(defun ft/message-vi-smart-mode ()
  "Switch to insert state if point is in a header line, to vi state otherwise

This is useful as a `gnus-message-setup-hook', to automatically choose
the proper mode for new messages and replies/followups."
  (cond ((message-point-in-header-p)
         (evil-insert-state))
        (t
         (evil-normal-state))))

(defun ft/message-attach-file ()
  "Attach a file to a message at the end of the buffer.

`mml-attach-file' works, too; but inserts the file tag anywhere it
pleases. This function makes sure those file tags are inserted at the
end of the text."
  (interactive)
  (goto-char (point-max))
  (setq file (mml-minibuffer-read-file "Attach file: "))
  (setq type (mml-minibuffer-read-type file))
  (setq description (mml-minibuffer-read-description))
  (mml-attach-file file type description "attachment"))

(defun ft/message-salutation-short-de ()
  (interactive) (insert "\nGruß Frank\n"))
(defun ft/message-salutation-long-de ()
  (interactive) (insert "\nGruß Frank Terbeck\n"))
(defun ft/message-salutation-short-en ()
  (interactive) (insert "\nRegards, Frank\n"))
(defun ft/message-salutation-long-en ()
  (interactive) (insert "\nKind Regards, Frank Terbeck\n"))

(defun ft/gnus-message-keys ()
  (mapc (lambda (m) (define-key m (kbd "TAB") 'ft/eudc-expand-inline))
        (list evil-normal-state-local-map
              evil-insert-state-local-map
              evil-replace-state-local-map))
  (define-key
    evil-normal-state-local-map (kbd "zA") 'ft/message-attach-file)
  (define-key
    evil-normal-state-local-map (kbd "zD") 'ft/spelling-pick-dictionary)
  (define-key
    evil-normal-state-local-map (kbd "zE") 'mml-secure-message-encrypt-pgpmime)
  (define-key
    evil-normal-state-local-map (kbd "zL") 'whitespace-mode)
  (define-key
    evil-normal-state-local-map (kbd "zN") 'mml-unsecure-message)
  (define-key
    evil-normal-state-local-map (kbd "zS") 'mml-secure-message-sign-pgpmime)
  (define-key
    evil-normal-state-local-map (kbd "zb") 'message-goto-body)
  (define-key
    evil-normal-state-local-map (kbd "zf") 'message-goto-from)
  (define-key
    evil-normal-state-local-map (kbd "zs") 'message-goto-subject)
  (define-key
    evil-normal-state-local-map (kbd "zt") 'message-goto-to)
  (define-key
    evil-normal-state-local-map (kbd "zgd") 'ft/message-salutation-short-de)
  (define-key
    evil-normal-state-local-map (kbd "zgD") 'ft/message-salutation-long-de)
  (define-key
    evil-normal-state-local-map (kbd "zge") 'ft/message-salutation-short-en)
  (define-key
    evil-normal-state-local-map (kbd "zgE") 'ft/message-salutation-long-en))

(add-hook 'message-mode-hook 'ft/gnus-message-keys)
(add-hook 'gnus-message-setup-hook 'ft/message-vi-smart-mode)

(defun ft/gnus-delete-article ()
  (interactive)
  (setq setnovice gnus-novice-user)
  (when setnovice (setq gnus-novice-user nil))
  (gnus-summary-delete-article)
  (when setnovice (setq gnus-novice-user t)))

(defun ft/gnus-summary-thread-collapse ()
  "Collapse the current thread entirely."
  (interactive)
  (gnus-summary-top-thread)
  (gnus-summary-hide-thread))

(defun ft/gnus-summary-thread-uncollapse ()
  "Uncollapse the current thread."
  (interactive)
  (gnus-summary-top-thread)
  (gnus-summary-show-thread))

(defun ft/gnus-summary-scroll-up-article ()
  "Scroll up the article buffer one line."
  (interactive)
  (gnus-summary-scroll-up -1))

(defun ft/gnus-summary-hide-article-or-quit ()
  "If in the summary buffer the article buffer is visible,
hide it. If is is not visible, exit the summary buffer."
  (interactive)
  (if (get-buffer-window gnus-article-buffer nil)
    (gnus-summary-expand-window)
    (gnus-summary-exit)))

(defun ft/gnus-select-summary-window ()
  "If in the summary buffer the article buffer is visible, move to it."
  (interactive)
  (let ((window (get-buffer-window gnus-summary-buffer nil)))
    (if window
        (select-window window))))

(defun ft/gnus-select-article-window ()
  "If in the summary buffer the article buffer is visible, move to it."
  (interactive)
  (let ((window (get-buffer-window gnus-article-buffer nil)))
    (if window
        (select-window window))))

(defun ft/gnus-summary-enter ()
  (when (not (gnus-summary-first-unread-subject))
    (end-of-buffer)
    (previous-line)
    (gnus-goto-colon)))

(defun ft/gnus-summary-keys ()
  (define-key gnus-summary-mode-map (kbd "M-a") 'ft/gnus-select-article-window)
  (define-key gnus-summary-mode-map (kbd "x") #'gnus-summary-next-page)
  (define-key gnus-summary-mode-map (kbd "q") 'ft/gnus-summary-hide-article-or-quit)
  (define-key gnus-summary-mode-map (kbd "DEL") 'ft/gnus-summary-scroll-up-article)
  (define-key gnus-summary-mode-map (kbd "TAB") 'gnus-summary-next-unread-subject)
  (define-key gnus-summary-mode-map (kbd "<backtab>") 'gnus-summary-prev-unread-subject)
  (define-key gnus-summary-mode-map (kbd "<home>") 'beginning-of-buffer)
  (define-key gnus-summary-mode-map (kbd "<end>") 'ft/gnus-buffer-last-line)
  (define-key gnus-summary-mode-map (kbd "<left>") 'ft/gnus-summary-thread-collapse)
  (define-key gnus-summary-mode-map (kbd "<right>") 'ft/gnus-summary-thread-uncollapse)
  (define-key gnus-summary-mode-map (kbd "<up>") 'gnus-summary-prev-thread)
  (define-key gnus-summary-mode-map (kbd "<down>") 'gnus-summary-next-thread)
  (define-key gnus-summary-mode-map (kbd "j") 'next-line)
  (define-key gnus-summary-mode-map (kbd "k") 'previous-line)
  (define-key gnus-summary-mode-map (kbd "vG") 'ft/gnus-getmail)
  (define-key gnus-summary-mode-map (kbd "vH") 'ft/gnus-bogo-train-ham)
  (define-key gnus-summary-mode-map (kbd "vJ") 'ft/gnus-bogo-train-spam)
  (define-key gnus-summary-mode-map (kbd "vU") 'gnus-summary-unmark-all-processable)
  (define-key gnus-summary-mode-map (kbd "vd") 'ft/gnus-delete-article)
  (define-key gnus-summary-mode-map (kbd "SPC") ft/space-map))

(add-hook 'gnus-summary-mode-hook 'ft/gnus-summary-keys)
(add-hook 'gnus-summary-prepared-hook 'ft/gnus-summary-enter)
(add-hook 'gnus-summary-mode-hook
          (lambda () (setq header-line-format
                           '(" « " mode-line-buffer-identification " »"))))

;; Thread sorting.
;;   First by number (which is the default), then by subject (to get git
;; patch series threads in order) and then - finally - sort root messages
;; by date to get a chronologically accurate display.
(setq gnus-thread-sort-functions
      '(gnus-thread-sort-by-number
        gnus-thread-sort-by-subject
        gnus-thread-sort-by-date))

(setq gnus-summary-thread-gathering-function
      'gnus-gather-threads-by-references)

(setq gnus-treat-display-smileys nil)
(setq gnus-treat-strip-cr t)
(setq gnus-thread-hide-killed t)

;; buffer layout
(gnus-add-configuration
 '(article '(vertical 1.0
                      (summary 0.20 point)
                      (article 1.0))))

;; In some modes I want the current line to be highlighted.
;; This function needs to be hooked into the right places to
;; do that.
(defun ft/gnus-setup-hl-line ()
  (hl-line-mode 1)
  (setq cursor-type nil))

(defun ft/gnus-article-fonts ()
  ;; The modus theme uses quite bright foreground colours. For my email expe-
  ;; rience, that is a little too much. Obviously emacs has got you covered and
  ;; you can remap faces to other faces, and if you make the alist parameter
  ;; buffer-local, this remapping is only active in that particular buffer.
  ;; Awesome!
  (set (make-local-variable 'face-remapping-alist)
       (copy-tree '((default ft/gnus-default))))
  (setq buffer-face-mode-face 'ft/gnus-article-face-size)
  (buffer-face-mode))

(require 'gnus-article-treat-patch)

;; This makes the part-patch treatment get used as soon as a line, which
;; looks like a hunk-start is found in the processed part.
(setq ft/gnus-article-patch-conditions
      '( "^@@ -[0-9]+,[0-9]+ \\+[0-9]+,[0-9]+ @@" ))

(setq gnus-treat-emphasize t)
(setq gnus-cite-minimum-match-count 1)

;; This is pretty much the default, but `}' needs to be in there, too.
(setq message-cite-prefix-regexp "\\([ \t]*[_.[:word:]]+>+\\|[ \t]*[]>|}]\\)+")

(setq gnus-visible-headers
      (concat "^"
              (regexp-opt
               '(
                 "User-Agent"
                 "X-Mailer"
                 "X-Mailreader"
                 "X-Newsreader"
                 "X-User-Agent"

                 "From"
                 "Reply-To"
                 "Followup-To"
                 "Mail-Followup-To"
                 "Mail-Copies-To"
                 "To"
                 "Cc"
                 "Bcc"
                 "Newsgroups"
                 "Posted-To"
                 "Message-Id"
                 "Archived-At"

                 "Subject"
                 "Date"
                 "Organization"

                 "Gnus-Warning"
                 "X-Now-Playing"
                 "X-Message-Sent-Securely"
                 "X-Redmine-Issue-Assignee"
                 )) ":"))

(setq gnus-header-face-alist '(("From" nil gnus-header-from)
                               ("Subject" nil gnus-header-subject)
                               ("To" nil ft/gnus-header-to)
                               ("Cc" nil ft/gnus-header-cc)
                               ("Newsgroups" nil gnus-header-newsgroups)
                               ("" gnus-header-name gnus-header-content)))

;; I always want topic mode to be on.
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)
(add-hook 'gnus-group-mode-hook
          (lambda () (setq header-line-format
                           '(" « " mode-line-buffer-identification " »"))))

;; Highlight the line in which the pointer is.
(add-hook 'gnus-group-mode-hook 'ft/gnus-setup-hl-line)

;; Indent topics/groups a little deeper.
(setq gnus-topic-indent-level 4)

;; So, the way that gnus presents its data to the user can be modified
;; by a vast number of formats. Let's take a walk through those...
;;
;; This one defines how the topic lines in topic mode look like.
;;   %i: inserts indention
;;   %n: the topic's name
;;   %v: visibility (which I currently don't know what that is...)
;;   %l: topic level
;;   %g: number of groups in within the topic
;;   %a: number of unread articles in the topic
;;   %A: like %a but also counts unread articles in subtopics
;;
;; Also, you can do stuff like %N{foobar%} which will paint "foobar" in
;; the face mentioned in gnus-face-0 (actually, the "gnus-face-" part is
;; not stable but let's keep it like that for now).
;; We'll use %0{...%} for painting the whole line (except the indention),
;; which we'll set to 'ft/gnus-face-topic-line'.
(setq gnus-topic-line-format "%i  %{[[ %(%n%): %A ]]%v%}\n")
(setq gnus-face-0 'ft/gnus-face-topic-line)

;; And this tells gnus how to present the group list to us. This has a
;; truckload of possible entities - so brace yourself:
;;   %M: '*' if the group has marked articles.
;;   %S: is the group subscribed?
;;   %L: level of "subscribedness"
;;   %N: number of unread articles
;;   %I: dormant
;;   %T: ticked
;;   %R: read
;;   %U: unseen
;;   %t: total number of articles (estimated)
;;   %y: number of unread, unticked, non-dormant articles
;;   %i: number of ticked and dormant articles
;;   %g: full group name
;;   %G: group name
;;   %C: group comment
;;   %D: description
;;   %o: 'm' if moderated
;;   %O: '(m)' if moderated
;;   %s: select method
;;   %B: is the summary buffer of this group open?
;;   %n: selected from where?
;;   %z: '<%s:%n>' for foreign groups
;;   %P: topic indention
;;   %c: short group name
;;   %m: mark if new mail arrived to this group lately
;;   %p: mark if the group is process marked
;;   %d: last read timestamp
;;   %F: disk space used by group
;;   %u: user defined specifier
(setq gnus-group-line-format "%S%p%P%~(ignore \"0\")~(pad 3)i%6y: %(%G%)\n")

;; which faces should we use for wich groups?
(setq gnus-group-highlight
      '(((>= ticked 1)  . ft/gnus-face-group-ticked)
        ((> unread 100) . ft/gnus-face-group-many)
        ((zerop unread) . ft/gnus-face-group-empty)
        (t . ft/gnus-face-group-some)))

;; Different date formats for different distances - timewise.
;; With this, you can just use %&user-date; in the summary format.
(setq gnus-user-date-format-alist
      '(((gnus-seconds-today) . "Today, %H:%M")
        ((+ 86400 (gnus-seconds-today)) . "Yesterday, %H:%M")
        (604800 . "%A, %H:%M")
        (t . "%d. %B '%y")))

;; Highlight the line in which the pointer is.
(add-hook 'gnus-summary-mode-hook 'ft/gnus-setup-hl-line)

;; Now for the meaty part, the Summary format (this controls how threads are
;; displayed). Earlier I said that the group line format recognized a truckload
;; of entities... Well, that was just a warm up. Now here's a long list for
;; your reading pleasure:
;;   %N: article number
;;   %S: subject string (with list identifiers stripped)
;;   %s: subject of the article if the root of the thread or the previous
;;       article had a different subject
;;   %F: From: header. The whole thing.
;;   %f: the 'To:' or the 'Newsgroups:' header
;;   %a: the name from the From: header (uses gnus-extract-address-components)
;;   %A: like %a, but inserts the *address* rather than the name
;;   %L: number of lines
;;   %c: number of characters
;;   %k: pretty printed version of %c (think: 1.2k)
;;   %I: indention based on threading level
;;   %B: a complex thread tree part
;;   %T: nothing if the article is root; lot's and lot's of spaces if it isn't
;;   %[: opening bracket, which is normally '[', but can also be '<' for adopted
;;       articles
;;   %]: same as %[ but closing
;;   %>: one space for each thread level
;;   %<: 20 minus thread level spaces
;;   %U: unread
;;   %R: says whether the article has been replied to, has been cached, or has
;;       been saved
;;   %i: score as a number
;;   %z: zcore...
;;       '+' if above the default level and '-' if below the default level. If
;;       the difference between 'gnus-summary-default-score' and the score is
;;       less than 'gnus-summary-zcore-fuzz', this spec will not be used.
;;   %V: total thread score
;;   %x: Xref:
;;   %D: Date:
;;   %d: the 'Date' in 'DD-MMM' format
;;   %o: the 'Date' in YYYYMMDD`T'HHMMSS format
;;   %M: Message-ID:
;;   %r: References:
;;   %t: number of articles in the subthread (this one is a little expensive)
;;   %e: 'gnus-not-empty-thread-mark' will be displayed if the message has
;;       children
;;   %P: line number
;;   %O: download mark
;;   %*: desired cursor position
;;   %&user-date;
;;       age sensitive date format; various date format is defined in
;;       'gnus-user-date-format-alist'
;;   %u: user defined specifier:
;;       The next character in the format string should be a letter. Gnus will
;;       call the function 'gnus-user-format-function-X', where X is the letter
;;       following '%u'. The function will be passed the current header as
;;       argument. The function should return a string, which will be inserted
;;       into the summary just like information from any other summary specifier.
;(setq gnus-summary-line-format "%O%U%R%z%d %(%[%4L: %-22,22f%]%) %B %s\n")
(setq gnus-summary-line-format "  %2{%-18,18&user-date;%}  %1{[ %-24,24uN ]%}  %3{%5i %U%R%} [%6,6L] %4{%3t%}: %B%s\n")

(defun gnus-user-format-function-N (header)
  (let* ((sender (ft/mail-header-sender header))
         (extra (mail-header-extra header))
         (redmine-sender* (assq 'X-Redmine-Sender extra))
         (redmine-sender (and redmine-sender* (cdr redmine-sender*))))
    (or redmine-sender sender)))

(setq gnus-face-1 'ft/gnus-face-summary-name)
(setq gnus-face-2 'ft/gnus-face-summary-date)
(setq gnus-face-3 'ft/gnus-face-summary-flags)
(setq gnus-face-4 'ft/gnus-face-summary-threadartn)

;; And here's one to modify the looks of the gnus entry in the mode line.
;;   %G: group name.
;;   %p: unprefixed group name.
;;   %A: current article number.
;;   %z: current article score.
;;   %V: gnus version.
;;   %U: number of unread articles in this group.
;;   %e: number of unread articles in this group that aren't displayed in
;;       the summary buffer.
;;   %Z: a string with the number of unread and unselected articles
;;       represented either as '<%U(+%e) more>' if there are both unread
(setq gnus-summary-mode-line-format
      "Gnus: %p #%A ★%z » ‹ %U unread ∘ %d dormant ∘ %t ticked ∘ %E expunged ›")

(setq gnus-article-mode-line-format "Gnus: %p: ‹ %S › ‹%w› %m")

(setq gnus-group-mode-line-format "Gnus: %M:%S")

;; If an article's subject is the same as the previous one, use this string,
;; to indicate that. Used by %s above. (Default: "")
(setq gnus-summary-same-subject "")

;; Remember how %B was a *complex* entry? Well, it is. The following variables
;; define what gets inserted and when:

;; Used for the root of a thread.  If `nil', use subject instead.
;; (Default: "> ")
(setq gnus-sum-thread-tree-root "")

;; Used for the false root of a thread. If `nil', use subject instead.
;; (Default: "> ")
(setq gnus-sum-thread-tree-false-root ">")

;; Used for a thread with just one message.  If `nil', use subject instead.
;; (Default: "")
(setq gnus-sum-thread-tree-single-indent "")

;; Used for a leaf with brothers. (Default: "+-> ")
(setq gnus-sum-thread-tree-leaf-with-other "├► ")

;; Used for indenting. (Default: "  ")
(setq gnus-sum-thread-tree-indent " ")

;; Used for drawing a vertical line. (Default: "| ")
(setq gnus-sum-thread-tree-vertical "│")

;; Used for a leaf without brothers. (Default: "\-> ")
(setq gnus-sum-thread-tree-single-leaf "╰► ")

;; Add a tree window, which visualises the thread once more. And paint it
;; horizontally, which I think uses screen estate a little better.
;(setq gnus-use-trees t)
;(setq gnus-generate-tree-function 'gnus-generate-horizontal-tree)

;; Group specific setup
;; gnus-parameters seems to use a winner-takes it all approach.
;; the first regex that matches is the one that gets used.
(setq gnus-parameters
      '(
        (":I[nN][bB][oO][xX]$"
         ;; I don't need much scoring inside my mail Inbox (yet?).
         (gnus-use-scoring nil)
         ;; My inbox isn't high-volume and I usually want to be able to
         ;; see and read old articles. So just display all articles. Always.
         (display . all)
         ;; I want articles I send of from my Inbox group to end up in it.
         ;; Otherwise threading will really not be as much fun as it could.
         (gcc-self . t))
        (":Spam\\(\\|-unsure\\)$"
         (gnus-show-threads nil)
         (display . all)
         (gnus-article-sort-functions '(gnus-article-sort-by-number
                                        gnus-article-sort-by-subject
                                        ft/gnus-article-sort-by-junk-header)))
        ("^nnmu+"
         (gnus-show-threads nil)
         (display . all)
         (gnus-article-sort-functions '(gnus-article-sort-by-subject
                                        gnus-article-sort-by-date)))
        (":work$"
         (gnus-use-scoring nil)
         (display . all)
         (gcc-self . t))
        ("^[^:]*:debian-\\(.*\\)$"  (to-list . "debian-\\1@lists.debian.org"))
        (":fdm-users$"              (to-list . "fdm-users@lists.sourceforge.net"))
        (":fvwm$"                   (to-list . "fvwm@fvwm.org"))
        (":fvwm-workers$"           (to-list . "fvwm-workers@fvwm.org"))
        (":git$"                    (to-list . "git@vger.kernel.org"))
        (":guix-help$"              (to-list . "help-guix@gnu.org"))
        (":guix-devel$"             (to-list . "guix-devel@gnu.org"))
        (":gnus$"                   (to-list . "ding@gnus.org"))
        (":grml$"                   (to-list . "grml@mur.at"))
        (":grml-devel$"             (to-list . "grml-devel@ml.grml.org"))
        ("^[^:]*:guile-\\(.*\\)$"   (to-list . "guile-\\1@gnu.org"))
        (":leafnode$"               (to-list . "leafnode-list@dt.e-technik.uni-dortmund.de"))
        (":linux-kernel$"           (to-list . "linux-kernel@vger.kernel.org"))
        (":octave-help$"            (to-list . "help-octave@octave.org"))
        ("^[^:]*:openbsd-\\(.*\\)$" (to-list . "\\1@openbsd.org"))
        ("^[^:]*:org-mode$"         (to-list . "emacs-orgmode@gnu.org"))
        ("^[^:]*:perl-beginners$"   (to-list . "beginners@perl.org"))
        ("^[^:]*:\\(perl-.*\\)$"    (to-list . "\\1@perl.org"))
        (":remind-fans$"            (to-list . "remind-fans@lists.roaringpenguin.com"))
        (":taglib-devel$"           (to-list . "taglib-devel@kde.org"))
        (":tmux-users$"             (to-list . "tmux-users@googlegroups.com"))
        (":vim-users$"              (to-list . "vim_use@googlegroups.com"))
        (":zsh-debian$"             (to-list . "pkg-zsh-devel@lists.alioth.debian.org"))
        (":zsh-users$"              (to-list . "zsh-users@zsh.org"))
        (":zsh-workers$"            (to-list . "zsh-workers@zsh.org"))
        (":xmms2$"                  (to-list . "xmms2-devel@lists.xmms.se"))
        (":xmonad$"                 (to-list . "xmonad@haskell.org"))))

;; I always want to see my Inbox. May it be empty or not.
(setq gnus-permanently-visible-groups "\\(Inbox\\|INBOX\\|work\\)$")

;; Everything shall go to Sent unless I said otherwise before.
(setq gnus-message-archive-group "nnimap+localmail:Sent")
(setq ft/gnus-inbox-group "nnimap+localmail:Inbox")
(setq ft/gnus-work-group "nnimap+localmail:work")

(defun ft/replace-gcc (value)
  (message-remove-header "gcc")
  (message-add-header (concat "Gcc: " value)))

(defun ft/magic-gcc ()
  (unless (message-fetch-field "in-reply-to")
    (save-excursion
      ;; Why this function? Well, before when I replied to a message in Inbox,
      ;; the Gcc: header was set to "Inbox". Great. But when I composed a *new*
      ;; mail from the Inbox group, that went to "Sent". Bummer.
      ;;
      ;; This function looks at the value of `gnus-newsgroup-name' (actually at
      ;; its default value, since it's set locally to "" before the
      ;; `gnus-message-setup-hook' is run) and if that's unset, look on which
      ;; group point rests in the *Group* buffer.
      ;;
      ;; That way, it's possible to control Gcc: for newly composed messages from
      ;; a summary buffer of a given group as well as from the group buffer.
      ;;
      ;; Now I can do "C-x m" to compose new message with point on "Inbox" in the
      ;; group buffer or in the summary buffer of the Inbox group and still get
      ;; the Gcc: header pointed at the "Inbox" group. Yay!
      ;;
      ;; If no special treatment is done, the default Gcc: header as determined
      ;; by the `gnus-message-archive-group' variable.
      (let* ((gnn (default-value 'gnus-newsgroup-name))
             (gap (save-excursion (switch-to-buffer "*Group*")
                                  (gnus-group-name-at-point)))
             (group-name (cond ((and gnn (not (string= gnn "")))
                                (gnus-group-decoded-name gnn))
                               ((and gap (not (string= gap "")))
                                (gnus-group-decoded-name gap))
                               (t nil))))
        (cond ((not group-name)
               ;; If the group name cannot be determined at all, assume that the
               ;; message is supposed to go to the "Inbox" group.
               (ft/replace-gcc ft/gnus-inbox-group))
              ((string-match ":I[nN][bB][oO][xX]$" group-name)
               (ft/replace-gcc ft/gnus-inbox-group))
              ((string-match ":work$" group-name)
               (ft/replace-gcc ft/gnus-work-group))
              ;; Nothing to do.
              (t t))))))

(add-hook 'gnus-message-setup-hook 'ft/magic-gcc)

;; Sensible from the the groups buffer directly
(setq user-full-name "Frank Terbeck")
(setq user-mail-address "ft@bewatermyfriend.org")

;; Posting styles are a powerful way to automagically guess
;; which attributes to compose a message with.
(setq gnus-posting-styles
      '(("."
         (name "Frank Terbeck")
         (address "ft@bewatermyfriend.org"))
        (message-news-p (signature-file "threadednewsreader.sig"))
        (message-mail-p (signature-file "rfc1925.sig"))
        ("^nnimap.*:grml.*$"           (address "ft@grml.org"))
        ((header "to" "ft@grml\\.org") (address "ft@grml.org"))
        ((header "cc" "ft@grml\\.org") (address "ft@grml.org"))
        ("^nnimap.*:zsh.*$"            (address "ft@zsh.org"))
        ((header "to" "ft@zsh\\.org")  (address "ft@zsh.org"))
        ((header "cc" "ft@zsh\\.org")  (address "ft@zsh.org"))
        ("^nnimap.*:work$"
         (address "terbeck@fh-aachen.de")
         (signature-file "work.sig"))
        ((header "to" "terbeck@fh-aachen\\.de")
         (address "terbeck@fh-aachen.de")
         (signature-file "work.sig"))
        ((header "cc" "terbeck@fh-aachen\\.de")
         (address "terbeck@fh-aachen.de")
         (signature-file "work.sig"))))

(require 'message)

;; Do not care of the if the from header look weird.
(add-to-list 'message-syntax-checks '(sender . disabled))

;; Create "John 'Dev' User <jdu@hackers.tld>" type from headers.
(setq mail-from-style 'angles)

;; Kill buffer after sending message
(setq message-kill-buffer-on-exit t)

;; When writing messages, put in show all headers gnus is going
;; to generate.
(setq message-generate-headers-first t)

;; Throw away (was: ...)
(setq message-subject-trailing-was-query t)

;; Yes, do cite the original when replying...
(setq message-cite-function 'message-cite-original-without-signature)

;; Insert my signature please.
(setq message-signature t)

;; Oh, and by the way, the files containing the signatures will be here.
(setq message-signature-directory "~/etc/mailnews/signatures")

(add-hook 'gnus-group-mode-hook 'gnus-group-sort-groups-by-rank)

;; If I want auto-filling within a mode, I can just add this to that
;; mode's hooks.
(defun ft/article-fill-column ()
  (turn-on-auto-fill)
  (set-fill-column 72))
(add-hook 'message-mode-hook 'ft/article-fill-column)

;; Spell checking. On the fly.
(defun ft/message-setup-spellcheck ()
  (flyspell-mode 1))
(add-hook 'message-mode-hook 'ft/message-setup-spellcheck)

(defun ft/mail-header-sender (header)
  (let* ((from (mail-extract-address-components (mail-header-from header)))
         (name (car from))
         (maybe-address (cdr from))
         (address (if (listp maybe-address)
                      (car maybe-address)
                    maybe-address)))
    (or name address "Someone")))

;; A proper (and short) attribution line.
(defun ft/message-attribution-simple-en ()
  (when message-reply-headers
    (insert (ft/mail-header-sender message-reply-headers) " wrote:\n")))

(setq message-citation-line-function 'ft/message-attribution-simple-en)

;; IMO, the *only* sane way to read mail using gnus is to connect to
;; an IMAP server. Since I otherwise want my mail to be available locally,
;; that requires me to have a server installed locally. dovecot does the
;; trick rather elegantly.
(setq nnimap-authinfo-file (concat ft/gnus-auth-dir "authinfo"))
(setq gnus-secondary-select-methods
      '((nnimap "localmail"
                (nnimap-address "localhost")
                (nnimap-stream ssl)
                (nnimap-authenticator login)
                (nnimap-server-port 10143)
                (nnimap-nov-is-evil t))))

;; localhost, where leafnode should be running. hopefully.
(setq gnus-select-method '(nntp "127.0.0.1"))

;;(require 'nnmairix)
;;
;;(setq nnmairix-group-prefix "zz_mairix")
;;(setq nnmairix-mairix-output-buffer "*mairix output*")
;;(setq nnmairix-customize-query-buffer "*mairix query*")
;;(setq nnmairix-mairix-update-options '("-F" "-Q"))
;;(setq nnmairix-mairix-search-options '("-Q"))
;;(setq nnmairix-mairix-synchronous-update nil)
;;(setq nnmairix-rename-files-for-nnml t)
;;(setq nnmairix-widget-fields-list
;;      '(
;;        ("from" "f" "From")
;;        ("to" "t" "To")
;;        ("cc" "c" "Cc")
;;        ("subject" "s" "Subject")
;;        ("to" "tc" "To or Cc")
;;        ("from" "a" "Address")
;;        (nil "b" "Body")
;;        (nil "n" "Attachment")
;;        ("Message-ID" "m" "Message ID")
;;        (nil "s" "Size")
;;        (nil "d" "Date")))
;;(setq nnmairix-widget-select-window-function
;;      (lambda () (select-window (get-largest-window))))
;;(setq nnmairix-propagate-marks-upon-close t)
;;(setq nnmairix-propagate-marks-to-nnmairix-groups nil)
;;(setq nnmairix-only-use-registry nil)
;;(setq nnmairix-allowfast-default nil)
;;
;;(setq nnmairix-widget-other '(threads flags))
;;(setq nnmairix-interactive-query-parameters
;;      '(
;;        (?f "from" "f" "From")
;;        (?t "to" "t" "To")
;;        (?c "to" "tc" "To or Cc")
;;        (?a "from" "a" "Address")
;;        (?s "subject" "s" "Subject")
;;        (?b nil "b" "Body")
;;        (?d nil "d" "Date")
;;        (?n nil "n" "Attachment")))
;;(setq nnmairix-delete-and-create-on-change '(nnimap nnmaildir nnml))

(setq gnus-use-scoring t)
(setq gnus-score-expiry-days nil)
(setq gnus-summary-default-score 0)
(setq gnus-kill-files-directory ft/gnus-score-dir)
(setq gnus-score-file-suffix "gnus-score")
(setq gnus-score-uncacheable-files "ADAPT$")
(setq gnus-save-score nil)
(setq gnus-score-over-mark "+")
(setq gnus-score-below-mark "-")

(setq gnus-home-score-file
      '(("." "ft.gnus-score")))

;; Like my spam-handling, my mail-fetching is done by `fdm'. I got a shell
;; script called `getmail.sh', that will call `fdm' with the appropriate
;; options and settings so all my mail is fetched. Usually, that script is
;; invoked by `cron' periodically, but sometimes I want to trigger fetches
;; manually, because I'm expecting something.

(defvar ft/gnus-getmail-sh
  "getmail.sh"
  "Name of the `getmail.sh' script; possibly the absolute path.")

(defvar ft/gnus-getmail-sh-buffer
  "*Output from getmail.sh*"
  "Name of the buffer to send the output from getmail.sh to.")

(defun ft/gnus-getmail ()
  "Invoke getmail.sh (as defined in `ft/gnus-getmail-sh'),
send the resulting output to `ft/gnus-getmail-sh-buffer' and
inform the user about the status in the minibuffer."
  (interactive)
  (let ((buf (with-current-buffer
                 (get-buffer-create ft/gnus-getmail-sh-buffer)
               (setq buffer-read-only nil)
               (goto-char (point-min))
               (when (not (= (point-min)
                             (point-max)))
                 (delete-forward-char (- (point-max) 1)))
               (current-buffer))))
    (message "Fetching new mail...")
    (call-process-shell-command ft/gnus-getmail-sh
                                nil
                                buf
                                t)
    (message "...done fetching new mail.")))

;; Here is how my Spam-handling works: I actually got `fdm' configured to do
;; the deed. It knows which spam-filter to use (`bogofilter' currently), it
;; knows where to move Spam and where to move false-positive Ham when
;; retraining. All that makes the job a lot simpler in here.
;;
;; What we need to do is provide commands, we can use to train the filter in
;; case it classified a mail wrongly. So we'll implement
;; `ft/gnus-bogo-train-spam' and `ft/gnus-bogo-train-ham', which can be bound
;; to key in `gnus-summary-mode-map' to have them available as shortcuts.
;;
;; In addition to that, this provides an article-sorting function, which can be
;; used in `gnus-article-sort-functions' to have messages with least spamicity
;; sorted to the top of the summary buffer.

(defvar ft/gnus-fdm
  "fdm"
  "Name of the `fdm' binary; possibly the absolute path.")

(defvar ft/gnus-fdm-buffer
  "*Output from fdm*"
  "Name of the buffer to send the output from fdm to.")

(defvar ft/gnus-fdm-ham-account
  "-atrain-good"
  "Account argument for `fdm' to train a message as spam.")

(defvar ft/gnus-fdm-spam-account
  "-atrain-spam"
  "Account argument for `fdm' to train a message as spam.")

(defvar ft/gnus-junk-header-regex "[A-Z]* - \\([0-9.]*\\)"
  "Regular expression matching the `ft/gnus-junk-header'. There
*must* be a parenthesized expression in this regex around the
numerical part of the junk header.")

(defvar ft/gnus-junk-header 'X-JunkFilter
  "Symbol of the junk header to process. This has to be added to
`gnus-extra-headers' as well.")

(defun ft/gnus-fdm-filter-spam (article-number spam)
  "If `spam' is `t', use `fdm' to train the current article as
spam. if `nil', train it as ham."
  (let ((article (save-window-excursion
                   (gnus-summary-goto-subject article-number)
                   (gnus-summary-show-article t)
                   (get-buffer gnus-article-buffer)))
        (buf (with-current-buffer
                 (get-buffer-create ft/gnus-fdm-buffer)
               (setq buffer-read-only nil)
               (goto-char (point-max))
               (current-buffer))))
    (with-temp-buffer
      (insert-buffer-substring article)
      (call-process-region (point-min)
                           (point-max)
                           ft/gnus-fdm
                           nil buf nil
                           (if spam
                               ft/gnus-fdm-spam-account
                             ft/gnus-fdm-ham-account)
                           "fetch"))))

(defun ft/gnus-process-bogo (n spam)
  "Worker for `ft/gnus-bogo-train-spam' and `ft/gnus-bogo-train-ham'.
This handles the process-mark behaviour. The functions using this
helper, do *not* remove process-marks, so trained articles can
be deleted when the training sequence is done."
  (let ((articles (sort
                    (copy-sequence (gnus-summary-work-articles n))
                    '<)))
    (save-excursion
      (while articles
        (ft/gnus-fdm-filter-spam (car articles) spam)
        (setq articles (cdr articles))))))

(defun ft/gnus-bogo-train-spam (&optional n)
  "Feed the current article into `fdm' to let it register and
handle it as spam. `fdm' needs to be properly configured in order
for this to work. In particular, it need to accept
`-atrain-spam', which will cause it to activate a disabled
account `train-spam'.

If the optional argument `n' is is given, train the next `n'
articles. If it is negative train the previous `n' articles. If
it is `nil' and articles in the summary are marked with the
process mark, the function works on those articles."
  (interactive "P")
  (ft/gnus-process-bogo n t))

(defun ft/gnus-bogo-train-ham (&optional n)
  "Feed the current article into `fdm' to let it register and
handle it as ham. `fdm' needs to be properly configured in order
for this to work. In particular, it need to accept
`-atrain-good', which will cause it to activate a disabled
account `train-good'.

If the optional argument `n' is is given, train the next `n'
articles. If it is negative train the previous `n' articles. If
it is `nil' and articles in the summary are marked with the
process mark, the function works on those articles."
  (interactive "P")
  (ft/gnus-process-bogo n nil))

(defun ft/gnus-header-junk-to-number (h)
  "Take a set of headers, pick `ft/gnus-junk-header' from it,
match it against `ft/gnus-junk-header-regex', transform the
result to a number and return that number.

If the configured header is not available in the message that is
being processed, return 0."
  (let ((value (gnus-extra-header ft/gnus-junk-header h)))
    (string-to-number
     (if (string-match ft/gnus-junk-header-regex value)
         (match-string 1 value )
       "0"))))

(defun ft/gnus-article-sort-by-junk-header (h1 h2)
  "Sort messages by a spam filter's junk header, as configured in
`ft/gnus-junk-header' and `ft/gnus-junk-header-regex'."
  (let ((junk-1 (ft/gnus-header-junk-to-number h1))
        (junk-2 (ft/gnus-header-junk-to-number h2)))
    (< junk-1 junk-2)))

;; Initialisation
(mapc (lambda (list)
        (add-to-list list ft/gnus-junk-header t)
        (add-to-list list 'X-Redmine-Issue-Id t)
        (add-to-list list 'X-Redmine-Issue-Assignee t)
        (add-to-list list 'X-Redmine-Sender t)
        (add-to-list list 'X-GitLab-NotificationReason t))
      '(nnmail-extra-headers
        gnus-extra-headers))

(message "Gnus config loading done.")
